# LaTeX2HTML 2K.1beta (1.62)
# Associate images original text with physical files.


$key = q/displaystylesqrt{overline{b_alpha^{,2}}-overline{b_alpha}^{,2}},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img419.gif"
 ALT="$\displaystyle \sqrt{ \overline{b_\alpha^{ 2}} - \overline{b_\alpha}^{ 2} },$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashmain_dialog.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="328" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img11.gif"
 ALT="\includegraphics[width=10cm]{Figures/main_dialog.eps}">|; 

$key = q/{displaymath}Delta^2_alpha(t)=6int_{0}^{t}dtau,(t-tau)C_{vv;alphaalpha}(tau).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="405" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img91.gif"
 ALT="\begin{displaymath}
\Delta^2_\alpha(t) = 6 \int_{0}^{t}d\tau (t - \tau)
C_{vv ; \alpha\alpha}(\tau).
\end{displaymath}">|; 

$key = q/{{bf{n}_{1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img131.gif"
 ALT="${\bf n}_{1}$">|; 

$key = q/includegraphics[width=5.2cm]{Figuresslashpreferences_file_handling.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="235" HEIGHT="198" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img28.gif"
 ALT="\includegraphics[width=5.2cm]{Figures/preferences_file_handling.eps}">|; 

$key = q/{displaymath}left(mathbf{sigma^{prime}}-lambda^{prime}mathbf{I}right)mathbf{eta}=0{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="339" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img317.gif"
 ALT="\begin{displaymath}
\left ( \mathbf{\sigma^{\prime}} - \lambda^{\prime} \mathbf{I}\right )\mathbf{\eta} = 0
\end{displaymath}">|; 

$key = q/Delta^2_alpha(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img507.gif"
 ALT="$\Delta^2_\alpha(t)$">|; 

$key = q/{displaymath}DOS^{(AR)}(omega)=frac{Deltat}{2}frac{k_BT}{M}mathrm{VACF}^{(AR)}left(exp[iomegaDeltat]right).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="460" HEIGHT="43" BORDER="0"
 SRC="|."$dir".q|img258.gif"
 ALT="\begin{displaymath}
DOS^{(AR)}(\omega)=\frac{\Delta t}{2}\frac{k_BT}{M} \mathrm{VACF}^{(AR)}
\left(\exp[i\omega\Delta t]\right).
\end{displaymath}">|; 

$key = q/{displaymath}EISF(q_m)doteqsum^{N_{species}}_{I=1}n_Iomega_IEISF_I(q_m),m=0ldotsN_q-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="489" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img523.gif"
 ALT="\begin{displaymath}
EISF(q_m) \doteq \sum^{N_{species}}_{I = 1} n_I \omega_I EISF_I(q_m), m = 0\ldots N_q - 1.
\end{displaymath}">|; 

$key = q/{{bf{1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img186.gif"
 ALT="${\bf 1}$">|; 

$key = q/mathrm{VACF}_{>}(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="88" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img269.gif"
 ALT="$\mathrm{VACF}_{&gt;}(z)$">|; 

$key = q/{displaymath}S_{IJ,mathrm{coh}}=delta_{IJ}+frac{1}{sqrt{n_In_I}}leftlanglesum_{a{sin(qr_{alphabeta})}{qr_{alphabeta}}rightrangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="440" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img532.gif"
 ALT="\begin{displaymath}
S_{IJ,\mathrm{coh}} = \delta_{IJ} + \frac{1}{\sqrt{n_In_I}}\...
...}
\frac{sin(qr_{\alpha\beta})}{qr_{\alpha\beta}}\right\rangle
\end{displaymath}">|; 

$key = q/m=-(N_t-1)ldotsN_t-1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="198" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img645.gif"
 ALT="$m = -(N_t-1)\ldots N_t-1$">|; 

$key = q/{displaymath}omega_{alpha}=frac{m_alpha}{sum_{alpha=1}^{N_{atoms}}m_alpha}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="348" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img34.gif"
 ALT="\begin{displaymath}
\omega_{\alpha}=\frac{m_\alpha}{\sum_{\alpha=1}^{N_{atoms}} m_\alpha}
\end{displaymath}">|; 

$key = q/2N_t-1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img647.gif"
 ALT="$2N_t - 1$">|; 

$key = q/n_{tg}(r_l,theta_m,phi_n)=n_{tg}(r_l,theta_m,phi_n)+1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="270" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img582.gif"
 ALT="$n_{tg}(r_l,\theta_m,\phi_n) = n_{tg}(r_l,\theta_m,\phi_n) + 1$">|; 

$key = q/sigma_nu=1slash(2pisigma_t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img670.gif"
 ALT="$\sigma_\nu = 1/(2\pi\sigma_t)$">|; 

$key = q/{displaymath}D_alpha=lim_{ttoinfty}frac{1}{6t}Delta^2_alpha(t).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="354" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img89.gif"
 ALT="\begin{displaymath}
D_\alpha = \lim_{t\to\infty} \frac{1}{6t} \Delta^2_\alpha(t).
\end{displaymath}">|; 

$key = q/x',y',z';MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img203.gif"
 ALT="$x',y',z'$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashplot_settings.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="652" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img623.gif"
 ALT="\includegraphics[width=10cm]{Figures/plot_settings.eps}">|; 

$key = q/tildeA^*left(frac{n}{2N_t}right)tildeBleft(frac{n}{2N_t}right);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="141" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img667.gif"
 ALT="$\tilde
A^*\left(\frac{n}{2N_t}\right) \tilde B\left(\frac{n}{2N_t}\right)$">|; 

$key = q/rho_J;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img541.gif"
 ALT="$\rho_J$">|; 

$key = q/{{bf{I}{{bf{J}={{bf{K};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img190.gif"
 ALT="${\bf I}{\bf J} = {\bf K}$">|; 

$key = q/{displaymath}VACF(t):=frac{langlev(t)v(0)rangle}{langlev^2(0)rangle}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="370" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img240.gif"
 ALT="\begin{displaymath}
VACF(t) := \frac{\langle v(t)v(0)\rangle}{\langle v^2(0)\rangle}
\end{displaymath}">|; 

$key = q/displaystylefrac{1}{n_I}sum^{n_I}_{alpha=1}langleexp[-i{{bf{q}cdothat{{bf{R}_alpha(t_0)]exp[i{{bf{q}cdothat{{bf{R}_alpha(t_0+t)]rangle_{t_0}.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="367" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img431.gif"
 ALT="$\displaystyle \frac{1}{n_I}\sum^{n_I}_{\alpha = 1}\langle\exp[-i{\bf q}
\cdot\hat{\bf R}_\alpha(t_0)]\exp[i{\bf q}\cdot\hat{\bf R}_\alpha(t_0+t)]\rangle_{t_0}.$">|; 

$key = q/Delta^2_alpha(t;{{bf{n});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img508.gif"
 ALT="$\Delta^2_\alpha(t;{\bf n})$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashanimation.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img632.gif"
 ALT="\includegraphics[width=10cm]{Figures/animation.eps}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdisfar.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="744" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img491.gif"
 ALT="\includegraphics[width=10cm]{Figures/disfar.eps}">|; 

$key = q/geq;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.gif"
 ALT="$\geq$">|; 

$key = q/E=hbar^2k^2slash2m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img396.gif"
 ALT="$E = \hbar^2 k^2/2m$">|; 

$key = q/d^2sigmaslashdOmegadE;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img390.gif"
 ALT="$d^2\sigma/d\Omega dE$">|; 

$key = q/{displaymath}{calF}^{g}_{mathrm{inc}}(q_m,kcdotDeltat)doteqsum^{N_{species}}_{I=,kcdotDeltat),qquadk=0ldotsN_t-1,;m=0ldotsN_q-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="647" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img502.gif"
 ALT="\begin{displaymath}
{\cal F}^{g}_{\mathrm{inc}}(q_m,k\cdot\Delta t) \doteq \sum^...
...t\Delta t),
\qquad k = 0\ldots N_t - 1,\; m = 0\ldots N_q - 1.
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashcomt.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="401" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img228.gif"
 ALT="\includegraphics[width=10cm]{Figures/comt.eps}">|; 

$key = q/textstyledoteq;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img156.gif"
 ALT="$\textstyle \doteq$">|; 

$key = q/q^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img501.gif"
 ALT="$q^2$">|; 

$key = q/{displaymath}{calF}_{mathrm{coh}}(q_m,kcdotDeltat)doteqsum^{N_{species}}_{I=1,Jgt)rangle}^{q},qquadk=0ldotsN_t-1,;m=0ldotsN_q-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="809" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img443.gif"
 ALT="\begin{displaymath}
{\cal F}_{\mathrm{coh}}(q_m,k\cdot\Delta t) \doteq \sum^{N_{...
...ngle}^{q},
\qquad k = 0\ldots N_t - 1,\; m = 0\ldots N_q - 1.
\end{displaymath}">|; 

$key = q/{displaymath}S(m)doteqsum_{k=0}^{N_t-m-1}[textbf{r}(k+m)-textbf{r}(k)]^2,qquadm=0ldotsN_t-1,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="495" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img97.gif"
 ALT="\begin{displaymath}
S(m) \doteq \sum_{k=0}^{N_t-m-1}[\textbf{r}(k+m) - \textbf{r}(k)]^2,
\qquad m = 0\ldots N_t-1,
\end{displaymath}">|; 

$key = q/displaystylefrac{{{bf{k}_0-{{bf{k}}{hbar},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img401.gif"
 ALT="$\displaystyle \frac{{\bf k}_0 - {\bf k}}{\hbar},$">|; 

$key = q/^{dag};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.gif"
 ALT="$^{\dag}$">|; 

$key = q/displaystylesum_{k=0}^{N_t-m-1}[textbf{r}^2(k+m)+textbf{r}^2(k)],;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="213" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img101.gif"
 ALT="$\displaystyle \sum_{k=0}^{N_t-m-1}[\textbf{r}^2(k+m) + \textbf{r}^2(k)],$">|; 

$key = q/displaystylefrac{1}{2pi}int_{-infty}^{+infty}dt,exp[-iomegat]C_{vv;alphaalpha}(t).;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="244" HEIGHT="60" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img147.gif"
 ALT="$\displaystyle \frac{1}{2\pi}\int_{-\infty}^{+\infty}dt  \exp[-i\omega t]
C_{vv ; \alpha\alpha}(t).$">|; 

$key = q/x(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img237.gif"
 ALT="$x(t)$">|; 

$key = q/n_{tg}(r_l,theta_m,phi_n);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="112" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img572.gif"
 ALT="$n_{tg}(r_l,\theta_m,\phi_n)$">|; 

$key = q/N_{species};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.gif"
 ALT="$N_{species}$">|; 

$key = q/omega_{I,mathrm{coh,inc}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img433.gif"
 ALT="$\omega_{I,\mathrm{coh,inc}}$">|; 

$key = q/{displaymath}sum_{I=1}^{N_{species}}n_Iomega_Isum^{nI}_{alpha=1}exp[-i{{bf{q}cdot{{bf{R}_alpha(nDeltat)]{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="414" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img490.gif"
 ALT="\begin{displaymath}
\sum_{I=1}^{N_{species}} n_I \omega_I \sum^{nI}_{\alpha=1} \exp[-i{\bf q}\cdot{\bf R}_\alpha(n\Delta t)]
\end{displaymath}">|; 

$key = q/S(q)={calF}_{mathrm{coh}}(q,t=0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="165" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img527.gif"
 ALT="$S(q) = {\cal F}_{\mathrm{coh}}(q, t = 0)$">|; 

$key = q/lambda_{min};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img224.gif"
 ALT="$\lambda_{min}$">|; 

$key = q/D=sum_alphaD_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img160.gif"
 ALT="$D = \sum_\alpha D_\alpha$">|; 

$key = q/omega_{I,mathrm{coh}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img530.gif"
 ALT="$\omega_{I,\mathrm{coh}}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashmsd_input_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="281" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img637.gif"
 ALT="\includegraphics[width=10cm]{Figures/msd_input_file.eps}">|; 

$key = q/b_{alpha,mathrm{coh}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img44.gif"
 ALT="$b_{\alpha,\mathrm{coh}}$">|; 

$key = q/theta_m=theta_{min}+mcdottheta_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img586.gif"
 ALT="$\theta_m = \theta_{min} + m \cdot \theta_{step}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashrog.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="401" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img126.gif"
 ALT="\includegraphics[width=10cm]{Figures/rog.eps}">|; 

$key = q/omega'_{i};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img383.gif"
 ALT="$\omega'_{i}$">|; 

$key = q/k_B;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img260.gif"
 ALT="$k_B$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdiscover_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img19.gif"
 ALT="\includegraphics[width=10cm]{Figures/discover_converter.eps}">|; 

$key = q/c^t;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img579.gif"
 ALT="$c^t$">|; 

$key = q/displaystyleb_{alpha,coh};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img416.gif"
 ALT="$\displaystyle b_{\alpha,coh}$">|; 

$key = q/displaystylesum_alphaomega_alpha{{bf{M}_alpha.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img212.gif"
 ALT="$\displaystyle \sum_\alpha \omega_\alpha {\bf M}_\alpha.$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdeuteration_selection_from_an_expression_string.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img71.gif"
 ALT="\includegraphics[width=10cm]{Figures/deuteration_selection_from_an_expression_string.eps}">|; 

$key = q/sigma_t=alphaslashT;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img161.gif"
 ALT="$\sigma_t=\alpha/T$">|; 

$key = q/{calC}_{ref};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img165.gif"
 ALT="${\cal C}_{ref}$">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_subplots.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img630.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_subplots.eps}">|; 

$key = q/displaystylesum_{k=0}^{N_t-1}textbf{r}^2(k).;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img108.gif"
 ALT="$\displaystyle \sum_{k=0}^{N_t-1} \textbf{r}^2(k).$">|; 

$key = q/{{bf{b}_1,{{bf{b}_2,{{bf{b}_3;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img451.gif"
 ALT="${\bf b}_1,{\bf b}_2,{\bf b}_3$">|; 

$key = q/xi_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img307.gif"
 ALT="$\xi_0$">|; 

$key = q/p({{bf{Omega}_0,t_0)=1slash(8pi^2);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="153" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img348.gif"
 ALT="$p({\bf\Omega}_0,t_0) = 1/(8\pi^2)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsubset_selection_from_the_loaded_trajectory.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img65.gif"
 ALT="\includegraphics[width=10cm]{Figures/subset_selection_from_the_loaded_trajectory.eps}">|; 

$key = q/displaystyle{{bf{q}cdot{{bf{M}{{bf{q},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img210.gif"
 ALT="$\displaystyle {\bf q}\cdot{\bf M}{\bf q},$">|; 

$key = q/{displaymath}{{bf{Delta}_alphadoteq{{bf{D}({{bf{q})left[{{bf{x}^{(0)}_alpha-{{bfX}^{(0)}right]-left[{{bf{x}_alpha-{{bf{X}right].{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="422" HEIGHT="37" BORDER="0"
 SRC="|."$dir".q|img171.gif"
 ALT="\begin{displaymath}
{\bf\Delta}_\alpha \doteq
{\bf D}({\bf q})\left[{\bf x}^{(0)...
...bf X}^{(0)}\right] -
\left[{\bf x}_\alpha - {\bf X}\right].
\end{displaymath}">|; 

$key = q/{displaymath}ROG(t)=sqrt{frac{sum_{alpha=1}^{N_{alpha}}({{bf{r}_{alpha}(t)-{{bf{r}_{cms}(t))}{N_{alpha}}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="418" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img123.gif"
 ALT="\begin{displaymath}
ROG(t) = \sqrt{\frac{\sum_{\alpha = 1}^{N_{\alpha}}({\bf r}_{\alpha}(t) - {\bf r}_{cms}(t))}{N_{\alpha}}}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslasheisf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="704" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img526.gif"
 ALT="\includegraphics[width=10cm]{Figures/eisf.eps}">|; 

$key = q/omega_{J,mathrm{coh}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img531.gif"
 ALT="$\omega_{J,\mathrm{coh}}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashrbt.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="521" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img225.gif"
 ALT="\includegraphics[width=10cm]{Figures/rbt.eps}">|; 

$key = q/{{bf{r}^{(0)}_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img215.gif"
 ALT="${\bf r}^{(0)}_\alpha$">|; 

$key = q/omega;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img316.gif"
 ALT="$\omega$">|; 

$key = q/phi_{min};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img593.gif"
 ALT="$\phi_{min}$">|; 

$key = q/{displaymath}lci=sum_{j=1}^{3N}Deltax_{ji}^4{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="333" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img324.gif"
 ALT="\begin{displaymath}
lci = \sum_{j = 1}^{3N} \Delta x_{ji}^4
\end{displaymath}">|; 

$key = q/displaystyleb_{alpha,mathrm{inc}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img48.gif"
 ALT="$\displaystyle b_{\alpha,\mathrm{inc}}$">|; 

$key = q/G(omega);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img143.gif"
 ALT="$G(\omega)$">|; 

$key = q/{displaymath}AC_i(t)=sum_{g=1}^{N_{triplets}}AC_{g,i}(t),qquadi=1,2,3{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="432" HEIGHT="63" BORDER="0"
 SRC="|."$dir".q|img135.gif"
 ALT="\begin{displaymath}
AC_i(t) = \sum_{g=1}^{N_{triplets}} AC_{g,i}(t), \qquad i = 1,2,3
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdos.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="624" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img163.gif"
 ALT="\includegraphics[width=10cm]{Figures/dos.eps}">|; 

$key = q/S_{AA+BB}(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.gif"
 ALT="$S_{AA+BB}(m)$">|; 

$key = q/displaystyle{{bf{r}^{(0)}_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img183.gif"
 ALT="$\displaystyle {\bf r}^{(0)}_\alpha$">|; 

$key = q/{{bf{K};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img189.gif"
 ALT="${\bf K}$">|; 

$key = q/{displaymath}c_{ab}(m)doteqleft{array{{ll}frac{1}{N_t-m}sum_{k=0}^{N_t-m-1}a^*(k(k)b(k-|m|),qquad&m=-(N_t-1)ldots-1.array{right.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="534" HEIGHT="78" BORDER="0"
 SRC="|."$dir".q|img644.gif"
 ALT="\begin{displaymath}
c_{ab}(m) \doteq \left\{
\begin{array}{ll}
\frac{1}{N_t - m}...
...m\vert),
\qquad &amp;m= -(N_t-1)\ldots -1.\\\\
\end{array} \right.
\end{displaymath}">|; 

$key = q/tildeBleft(frac{n}{2N_t}right);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img662.gif"
 ALT="$\tilde
B\left(\frac{n}{2N_t}\right)$">|; 

$key = q/r(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img236.gif"
 ALT="$r(t)$">|; 

$key = q/{displaymath}C_{vv}^{>}(z)=sum_{j=1}^Pbeta_jfrac{z}{z-z_j}cdotlanglev^2rangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="384" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img295.gif"
 ALT="\begin{displaymath}
C_{vv}^{&gt;}(z)= \sum_{j=1}^P\beta_j\frac{z}{z-z_j}\cdot\langle v^2\rangle
\end{displaymath}">|; 

$key = q/VACF(n);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img277.gif"
 ALT="$VACF(n)$">|; 

$key = q/beta;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img534.gif"
 ALT="$\beta$">|; 

$key = q/displaystylefrac{E_0-E}{hbar}.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img403.gif"
 ALT="$\displaystyle \frac{E_0 - E}{\hbar}.$">|; 

$key = q/{{bf{q}=(q_0,q_1,q_2,q_3);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img173.gif"
 ALT="${\bf q} = (q_0,q_1,q_2,q_3)$">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_back.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img626.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_back.eps}">|; 

$key = q/C_{ii}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img605.gif"
 ALT="$C_{ii}(t)$">|; 

$key = q/kequiv|{{bf{k}|;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img394.gif"
 ALT="$k\equiv\vert{\bf k}\vert$">|; 

$key = q/{displaymath}p^j_{mn}(t)=intdOmega,p({{bf{Omega},t|{{bf{0},0)D^{j}_{mn}({{bf{Omega}).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="413" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img353.gif"
 ALT="\begin{displaymath}
p^j_{m n}(t) = \int d\Omega p({\bf\Omega},t\vert{\bf0},0)
D^{j}_{m n}({\bf\Omega}).
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashcn.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="531" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img564.gif"
 ALT="\includegraphics[width=10cm]{Figures/cn.eps}">|; 

$key = q/mathbf{M};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img320.gif"
 ALT="$\mathbf{M}$">|; 

$key = q/N_{G};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img557.gif"
 ALT="$N_{G}$">|; 

$key = q/{displaymath}S_{mathrm{coh}}(q)=sum^{N_{species}}_{I,JgeI}sqrt{n_In_Jomega_{I,mathrm{coh}}omega_{J,mathrm{coh}}}S_{IJ}(q){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="438" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img529.gif"
 ALT="\begin{displaymath}
S_{\mathrm{coh}}(q) = \sum^{N_{species}}_{I,J \ge I}\sqrt{n_I n_J \omega_{I,\mathrm{coh}}\omega_{J,\mathrm{coh}}} S_{IJ}(q)
\end{displaymath}">|; 

$key = q/displaystyleb_{alpha,inc};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img418.gif"
 ALT="$\displaystyle b_{\alpha,inc}$">|; 

$key = q/{{bf{v}_1=frac{{{bf{n}_{1}+{{bf{n}_{2}}{||{{bf{n}_{1}+{{bf{n}_{2}||};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img130.gif"
 ALT="${\bf v}_1 = \frac{{\bf n}_{1} + {\bf n}_{2}}{\vert\vert{\bf n}_{1} + {\bf n}_{2}\vert\vert}$">|; 

$key = q/phi_n;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img568.gif"
 ALT="$\phi_n$">|; 

$key = q/{displaymath}{calW_I}=frac{n_{I}b_{I,mathrm{inc}}^2}{sum^{N_{species}}_{I=1}b_{I,mathrm{inc}}^2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="360" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img57.gif"
 ALT="\begin{displaymath}
{\cal W_I}=\frac{n_{I}b_{I,\mathrm{inc}}^2}{\sum^{N_{species}}_{I=1} b_{I,\mathrm{inc}}^2}
\end{displaymath}">|; 

$key = q/tr;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img199.gif"
 ALT="$tr$">|; 

$key = q/{{bf{R}';MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="21" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img201.gif"
 ALT="${\bf R}'$">|; 

$key = q/S_{AB}(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.gif"
 ALT="$S_{AB}(m)$">|; 

$key = q/c_{aa}(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img649.gif"
 ALT="$c_{aa}(m)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdisfg.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="584" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img510.gif"
 ALT="\includegraphics[width=10cm]{Figures/disfg.eps}">|; 

$key = q/q_{min};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img466.gif"
 ALT="$q_{min}$">|; 

$key = q/{{bf{Q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img197.gif"
 ALT="${\bf Q}$">|; 

$key = q/dpsislashdt;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img284.gif"
 ALT="$d\psi/dt$">|; 

$key = q/{Psi_{>}(z)};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img301.gif"
 ALT="${\Psi_{&gt;}(z)}$">|; 

$key = q/N_{groups};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img226.gif"
 ALT="$N_{groups}$">|; 

$key = q/N_{bonds};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img614.gif"
 ALT="$N_{bonds}$">|; 

$key = q/{displaymath}MSD^{AR}(n)stackrel{nrightarrow+infty}{simeq}2DnDeltatmbox{;}D=Deltfrac{beta_j}{1-z_j}=frac{langlev^2rangle}{gamma}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="501" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img299.gif"
 ALT="\begin{displaymath}
MSD^{AR}(n)\stackrel{n\rightarrow +\infty}{\simeq} 2Dn\Delta...
...{j=1}^P\frac{\beta_j}{1-z_j}=\frac{\langle v^2\rangle}{\gamma}
\end{displaymath}">|; 

$key = q/r^H_{i,k};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img618.gif"
 ALT="$r^H_{i,k}$">|; 

$key = q/{displaymath}F_{I,inc,alpha}(q_m,kcdotDeltat)=sum_{alpha=1}^{n_I}overline{langlea(0)]exp[i{{bf{q}cdot{{bf{R}_alpha(t)]rangle}^q.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="507" HEIGHT="56" BORDER="0"
 SRC="|."$dir".q|img485.gif"
 ALT="\begin{displaymath}
F_{I, inc,\alpha}(q_m,k\cdot\Delta t) = \sum_{\alpha=1}^{n_I...
...f R}_\alpha(0)]\exp[i{\bf q}\cdot{\bf R}_\alpha(t)]\rangle}^q.
\end{displaymath}">|; 

$key = q/displaystylea(kcdotDeltat),qquadb(kcdotDeltat),qquadk=0ldotsN_t-1,;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="344" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img640.gif"
 ALT="$\displaystyle a(k\cdot\Delta t),\qquad b(k\cdot\Delta t), \qquad k = 0\ldots N_t-1,$">|; 

$key = q/q1_x,q1_y,q1_z;q2_x,q2_y,q2_z;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="196" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img471.gif"
 ALT="$q1_x,q1_y,q1_z;q2_x,q2_y,q2_z$">|; 

$key = q/{displaymath}{calF}_{mathrm{inc}}({{bf{q},t)=EISF({{bf{q})+{calF}_{mathrm{inc}}'({{bf{q},t),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="410" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img512.gif"
 ALT="\begin{displaymath}
{\cal F}_{\mathrm{inc}}({\bf q},t) = EISF({\bf q}) + {\cal F}_{\mathrm{inc}}'({\bf q},t),
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashabout_nmoldyn.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="317" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img635.gif"
 ALT="\includegraphics[width=10cm]{Figures/about_nmoldyn.eps}">|; 

$key = q/displaystylerho_{alpha,2}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img497.gif"
 ALT="$\displaystyle \rho_{\alpha,2}(t)$">|; 

$key = q/{displaymath}{{bf{A}=a_0cdot{{bf{1}+a_1cdot{{bf{I}+a_2cdot{{bf{J}+a_3cdot{{bf{K},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="415" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img191.gif"
 ALT="\begin{displaymath}
{\bf A} = a_0\cdot{\bf 1} + a_1\cdot{\bf I} + a_2\cdot{\bf J} +
a_3\cdot{\bf K},
\end{displaymath}">|; 

$key = q/Gamma_{alphabeta};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img415.gif"
 ALT="$\Gamma_{\alpha\beta}$">|; 

$key = q/r(0)=VACF(0)=1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="169" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img241.gif"
 ALT="$r(0)=VACF(0)=1$">|; 

$key = q/{displaymath}sigma_P^2=r(0)-sum_{n=1}^Pa_n^{(P)}mathrm(nDeltat),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="383" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img235.gif"
 ALT="\begin{displaymath}
\sigma_P^2=r(0)-\sum_{n=1}^P a_n^{(P)} \mathrm(n\Delta t),
\end{displaymath}">|; 

$key = q/Deltax;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img315.gif"
 ALT="$\Delta x$">|; 

$key = q/{calS}({{bf{q},omega);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img389.gif"
 ALT="${\cal S}({\bf q},\omega)$">|; 

$key = q/{displaymath}D_alpha=int_{0}^{t}dtau,C_{vv;alphaalpha}(tau).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="364" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img92.gif"
 ALT="\begin{displaymath}
D_\alpha = \int_{0}^{t}d\tau  C_{vv ; \alpha\alpha}(\tau).
\end{displaymath}">|; 

$key = q/tin[0,NDeltat];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img280.gif"
 ALT="$t\in [0,N\Delta t]$">|; 

$key = q/{{bf{Omega}_1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img338.gif"
 ALT="${\bf\Omega}_1$">|; 

$key = q/[phi,phi+dphi];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img575.gif"
 ALT="$[\phi , \phi + d\phi]$">|; 

$key = q/^{d};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.gif"
 ALT="$^{d}$">|; 

$key = q/I;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img61.gif"
 ALT="$I$">|; 

$key = q/M;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img259.gif"
 ALT="$M$">|; 

$key = q/displaystylep^j_{mn}(0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img372.gif"
 ALT="$\displaystyle p^j_{m n}(0)$">|; 

$key = q/{displaymath}mathbf{Deltax}=mathbf{M}^{-1slash2}mathbf{eta}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="336" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img322.gif"
 ALT="\begin{displaymath}
\mathbf{\Delta x} = \mathbf{M}^{-1/2}\mathbf{\eta}
\end{displaymath}">|; 

$key = q/includegraphics[width=0.60textwidth]{.slashFiguresslashlogo.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<A NAME="tex2html_wrap16373"
 HREF="img6.ps">[IMAGE gif]</A>|; 

$key = q/{displaymath}TCF(r)=4pirrho_0(PDF(r)-1.0){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="405" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img544.gif"
 ALT="\begin{displaymath}
TCF(r) = 4\pi r \rho_0 (PDF(r) - 1.0)
\end{displaymath}">|; 

$key = q/{displaymath}S^2_i=frac{1}{N_{frames}}sum_{i=1}^{N_{frames}}S^2_i(t_i){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="385" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img620.gif"
 ALT="\begin{displaymath}
S^2_i = \frac{1}{N_{frames}}\sum_{i=1}^{N_{frames}} S^2_i(t_i)
\end{displaymath}">|; 

$key = q/displaystyle{calF}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img408.gif"
 ALT="$\displaystyle {\cal F}({\bf q},t)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdcsf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="744" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img463.gif"
 ALT="\includegraphics[width=10cm]{Figures/dcsf.eps}">|; 

$key = q/displaystylesqrt{overline{b_alpha^2}-overline{b_alpha}^2};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img49.gif"
 ALT="$\displaystyle \sqrt{\overline{b_\alpha^2}-\overline{b_\alpha}^2}$">|; 

$key = q/hat{{bf{R}_alpha(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img412.gif"
 ALT="$\hat {\bf R}_\alpha(t)$">|; 

$key = q/{displaymath}{{bf{R}=x'{{bf{b}_1+y'{{bf{b}_2+z'{{bf{b}_3,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="372" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img452.gif"
 ALT="\begin{displaymath}
{\bf R} = x'{\bf b}_1 + y'{\bf b}_2 + z'{\bf b}_3,
\end{displaymath}">|; 

$key = q/{displaymath}m({{bf{q};{{bf{X},{{bf{X}^{(0)})=sum_alphaomega_alpha|{{bf{Delta}|^2_alpha.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="386" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img175.gif"
 ALT="\begin{displaymath}
m({\bf q};{\bf X},{\bf X}^{(0)}) =
\sum_\alpha \omega_\alpha \vert{\bf\Delta}\vert^2_\alpha.
\end{displaymath}">|; 

$key = q/displaystyle{{bf{X};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img176.gif"
 ALT="$\displaystyle {\bf X}$">|; 

$key = q/displaystylesum^{N_{species}}_{I,JgeI}sqrt{n_In_Jomega_{I,mathrm{coh}}omega_{J,mathrm{coh}}}{calF}_{IJ,mathrm{coh}}({{bf{q},t),;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="291" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img425.gif"
 ALT="$\displaystyle \sum^{N_{species}}_{I,J \ge I}\sqrt{n_I n_J\omega_{I,\mathrm{coh}}\omega_{J,\mathrm{coh}}}{\cal F}_{IJ,\mathrm{coh}}({\bf q},t),$">|; 

$key = q/{displaymath}p^j_{mn}(t)=frac{1}{N}sum_alphalangleD^{j}_{mn}({{bf{Omega}_alpha(t))rangle.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="394" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img357.gif"
 ALT="\begin{displaymath}
p^j_{m n}(t) = \frac{1}{N}
\sum_\alpha \langle D^{j}_{m n}({\bf\Omega}_\alpha(t))\rangle.
\end{displaymath}">|; 

$key = q/q_m=q_{min}+mcdotDeltaq;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="156" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img449.gif"
 ALT="$q_m = q_{min} + m\cdot\Delta q$">|; 

$key = q/phi_{max};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img596.gif"
 ALT="$\phi_{max}$">|; 

$key = q/[r,r+dr];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img573.gif"
 ALT="$[r, r+dr]$">|; 

$key = q/{displaymath}RMSD(ncdotDeltat)=sqrt{frac{sum_{alpha=1}^{N_{alpha}}({{bf{r}_{alphbf{r}_{ref}(t))}{N_{alpha}}},qquadn=0ldotsN_t-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="525" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img120.gif"
 ALT="\begin{displaymath}
RMSD(n\cdot\Delta t) = \sqrt{\frac{\sum_{\alpha = 1}^{N_{\al...
...- {\bf r}_{ref}(t))}{N_{\alpha}}},
\qquad n = 0\ldots N_t - 1.
\end{displaymath}">|; 

$key = q/{displaymath}A_a(t)=sum_{i=1}^{3N}Deltax_{ai}M_i^{frac{1}{2}}left(x_i(t)-langlex_irangleright){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="409" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img330.gif"
 ALT="\begin{displaymath}
A_a(t) = \sum_{i=1}^{3N} \Delta x_{ai} M_i^{\frac{1}{2}} \left(x_i(t) - \langle x_i\rangle\right)
\end{displaymath}">|; 

$key = q/{displaymath}C_{omegaomega}(t;i)doteqlangleomega'_{i}(0)omega_{i}'(t)rangle.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="370" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img384.gif"
 ALT="\begin{displaymath}
C_{\omega\omega}(t;i) \doteq
\langle\omega'_{i}(0)\omega_{i}'(t) \rangle.
\end{displaymath}">|; 

$key = q/theta_m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img567.gif"
 ALT="$\theta_m$">|; 

$key = q/phi;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img571.gif"
 ALT="$\phi$">|; 

$key = q/{displaymath}{{bf{R}'=x'cdot{{bf{I}+y'cdot{{bf{J}+z'cdot{{bf{K},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="382" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img202.gif"
 ALT="\begin{displaymath}
{\bf R}' = x'\cdot{\bf I} + y'\cdot{\bf J} + z'\cdot{\bf K},
\end{displaymath}">|; 

$key = q/displaystylefrac{1}{N}sum_{alpha,beta}b_{alpha,coh},b_{beta,coh}langleexp[-i{{bfat{{bf{R}_alpha(0)]exp[i{{bf{q}cdothat{{bf{R}_beta(t)]rangle,;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="396" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img421.gif"
 ALT="$\displaystyle \frac{1}{N}\sum_{\alpha,\beta}
b_{\alpha,coh} b_{\beta,coh}
\lan...
...f q}\cdot\hat{\bf R}_\alpha(0)]
\exp[i{\bf q}\cdot\hat{\bf R}_\beta(t)]\rangle,$">|; 

$key = q/N_{shell};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img468.gif"
 ALT="$N_{shell}$">|; 

$key = q/q2_x,q2_y,q2_z;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img473.gif"
 ALT="$q2_x,q2_y,q2_z$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsfa.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="356" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img599.gif"
 ALT="\includegraphics[width=10cm]{Figures/sfa.eps}">|; 

$key = q/n_{alphaJ}(r);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img542.gif"
 ALT="$n_{\alpha J}(r)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashpreferences_file_handling.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="452" HEIGHT="380" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img27.gif"
 ALT="\includegraphics[width=10cm]{Figures/preferences_file_handling.eps}">|; 

$key = q/nm^2s^{-2};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img154.gif"
 ALT="$nm^2s^{-2}$">|; 

$key = q/{displaymath}m'({{bf{q},lambda)={{bf{q}cdot{{bf{M}{{bf{q}-lambda({{bf{q}cdot{{bf{q}-1)stackrel{!}{=}Min.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="435" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img217.gif"
 ALT="\begin{displaymath}
m'({\bf q},\lambda) = {\bf q}\cdot{\bf M}{\bf q}
- \lambda({\bf q}\cdot{\bf q} - 1) \stackrel{!}{=} Min.
\end{displaymath}">|; 

$key = q/displaystyle1.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img221.gif"
 ALT="$\displaystyle 1.$">|; 

$key = q/{{bf{J};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img188.gif"
 ALT="${\bf J}$">|; 

$key = q/{displaymath}mathbf{sigma^{prime}}=mathbf{M^{1slash2}}mathbf{sigma}mathbf{M^{1slash2}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="27" BORDER="0"
 SRC="|."$dir".q|img319.gif"
 ALT="\begin{displaymath}
\mathbf{\sigma^{\prime}} = \mathbf{M^{1/2}} \mathbf{\sigma} \mathbf{M^{1/2}}
\end{displaymath}">|; 

$key = q/displaystylefrac{1}{n_I}sum^{n_I}_{alpha}expleft[-frac{(q_m)^2}{2}Delta^2_alpha(cdotDeltat;{{bf{n})right]quadmbox{{{rm{non-isotropicsystem}}.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="446" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img505.gif"
 ALT="$\displaystyle \frac{1}{n_I}\sum^{n_I}_{\alpha} \exp\left[-\frac{(q_m)^2}{2}
\De...
...2_\alpha(k\cdot\Delta t;{\bf n})\right]
\quad\mbox{{\rm non-isotropic system}}.$">|; 

$key = q/f(n)=frac{1}{2pii}oint_Cdz,z^{n-1}F(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="204" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img250.gif"
 ALT="$f(n) = \frac{1}{2\pi i}\oint_C dz z^{n-1}
F(z)$">|; 

$key = q/mathcal{F}_{mathrm{inc}}(q,t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img509.gif"
 ALT="$\mathcal{F}_{\mathrm{inc}}(q,t)$">|; 

$key = q/2pi;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img459.gif"
 ALT="$2\pi$">|; 

$key = q/{displaymath}frac{d^{2}sigma}{dOmegadE}=Ncdotfrac{k}{k_0}{calS}({{bf{q},omega).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="370" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img393.gif"
 ALT="\begin{displaymath}
\frac{d^{2}\sigma}{d\Omega dE} = N\cdot\frac{k}{k_0}{\cal S}({\bf q},\omega).
\end{displaymath}">|; 

$key = q/VACF(0)=1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="115" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img265.gif"
 ALT="$VACF(0)=1$">|; 

$key = q/textbf{R}_alpha(t_0+t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img84.gif"
 ALT="$\textbf{R}_\alpha(t_0 + t)$">|; 

$key = q/z_j;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img303.gif"
 ALT="$z_j$">|; 

$key = q/{displaymath}PDF_{IJ}(r)=frac{leftlanglesum_{alpha=1}^{n_I}n_{alphaJ}(r)rightrangle}{n_Irho_J4pir^2dr}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="388" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img540.gif"
 ALT="\begin{displaymath}
PDF_{IJ}(r) = \frac{\left\langle\sum_{\alpha = 1}^{n_I} n_{\alpha J}(r)\right\rangle}{n_I\rho_J 4\pi r^2dr}
\end{displaymath}">|; 

$key = q/omega_{J,mathrm{coh,inc}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img434.gif"
 ALT="$\omega_{J,\mathrm{coh,inc}}$">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_home.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img625.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_home.eps}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashara.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="624" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img310.gif"
 ALT="\includegraphics[width=10cm]{Figures/ara.eps}">|; 

$key = q/displaystyleS(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img98.gif"
 ALT="$\displaystyle S(m)$">|; 

$key = q/displaystyle{calF}_{inc}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="77" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img422.gif"
 ALT="$\displaystyle {\cal F}_{inc}({\bf q},t)$">|; 

$key = q/rho;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img598.gif"
 ALT="$\rho$">|; 

$key = q/tau_{eff,i};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img611.gif"
 ALT="$\tau_{eff,i}$">|; 

$key = q/sigma;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img313.gif"
 ALT="$\sigma$">|; 

$key = q/{{bf{n}_{2};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img132.gif"
 ALT="${\bf n}_{2}$">|; 

$key = q/{displaymath}array{{ll}&D^{j}_{mn}({{bf{q})=sum_{p}(-1)^{p}frac{left[(j+m)!(j-m))^{j-n-p}(q_2+iq_1)^{p+n-m}(q_2-iq_1)^{p}.array{{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="530" HEIGHT="81" BORDER="0"
 SRC="|."$dir".q|img369.gif"
 ALT="\begin{displaymath}
\begin{array}{ll}
&amp;D^{j}_{m n}({\bf q}) =
\sum_{p}(-1)^{p...
..._0-iq_3)^{j-n-p}
(q_2+iq_1)^{p+n-m}(q_2-iq_1)^{p}.
\end{array}\end{displaymath}">|; 

$key = q/q_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img467.gif"
 ALT="$q_{step}$">|; 

$key = q/displaystyleG(omega);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img144.gif"
 ALT="$\displaystyle G(\omega)$">|; 

$key = q/mathrm{VACF}^{(AR)}(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img256.gif"
 ALT="$\mathrm{VACF}^{(AR)}(z)$">|; 

$key = q/S^2_i=C_{ii}(+infty);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img610.gif"
 ALT="$S^2_i =C_{ii}(+\infty)$">|; 

$key = q/xi({bf{{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img488.gif"
 ALT="$\xi(\bf {q},t)$">|; 

$key = q/{displaymath}C_{vv;alphaalpha}(t;{{bf{n})doteqlanglev_alpha(t_0;{{bf{n})v_alpha(t_0+t;{{bf{n})rangle_{t_0},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="429" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img139.gif"
 ALT="\begin{displaymath}
C_{vv ; \alpha\alpha}(t;{\bf n}) \doteq
\langle v_\alpha(t_0;{\bf n})v_\alpha(t_0+t;{\bf n})\rangle_{t_0},
\end{displaymath}">|; 

$key = q/{displaymath}sqrt{omega_{alpha}}=frac{b_{alpha,mathrm{coh}}}{sqrt{sum_{alpha=1}^{N_{atoms}}b_{alpha,mathrm{coh}}^2}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="372" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img37.gif"
 ALT="\begin{displaymath}
\sqrt{\omega_{\alpha}}=\frac{b_{\alpha,\mathrm{coh}}}{\sqrt{\sum_{\alpha=1}^{N_{atoms}} b_{\alpha,\mathrm{coh}}^2}}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashrmsd.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="544" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img122.gif"
 ALT="\includegraphics[width=10cm]{Figures/rmsd.eps}">|; 

$key = q/0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img453.gif"
 ALT="$0$">|; 

$key = q/{displaymath}EISF_I({{bf{q})=frac{1}{n_I}sum^{n_I}_{alpha}langle|exp[i{{bf{q}cdot{{bf{R}_alpha]|^2rangle.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="418" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img521.gif"
 ALT="\begin{displaymath}
EISF_I({\bf q}) = \frac{1}{n_I}\sum^{n_I}_{\alpha} \langle\vert\exp[i{\bf q}\cdot{\bf R}_\alpha]\vert^2\rangle.
\end{displaymath}">|; 

$key = q/m_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.gif"
 ALT="$m_\alpha$">|; 

$key = q/c_{ab}(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img646.gif"
 ALT="$c_{ab}(m)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdlpoly_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img17.gif"
 ALT="\includegraphics[width=10cm]{Figures/dlpoly_converter.eps}">|; 

$key = q/sum_{alpha=1}^Nomega_alpha=1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="105" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.gif"
 ALT="$\sum_{\alpha=1}^N \omega_\alpha=1$">|; 

$key = q/a^{(P)}_n;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img261.gif"
 ALT="$a^{(P)}_n$">|; 

$key = q/N_{shell}=E(frac{q_{max}-q_{min}}{q_{step}})+1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="204" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img469.gif"
 ALT="$N_{shell}= E(\frac{q_{max} - q_{min}}{q_{step}}) + 1$">|; 

$key = q/langleldotsrangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img356.gif"
 ALT="$\langle\ldots\rangle$">|; 

$key = q/Deltanu=1slash(2N_tDeltat);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img158.gif"
 ALT="$\Delta\nu = 1/(2N_t \Delta t)$">|; 

$key = q/{displaymath}{calS}({{bf{q})doteqint_{-infty}^{+infty}domega,{calS}_{coh}(q,omega)={calF}_{coh}({{bf{q},0).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="430" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img435.gif"
 ALT="\begin{displaymath}
{\cal S}({\bf q}) \doteq \int_{-\infty}^{+\infty}d\omega 
{\cal S}_{coh}(q,\omega) = {\cal F}_{coh}({\bf q},0).
\end{displaymath}">|; 

$key = q/{displaymath}lim_{qto0}frac{omega^2}{q^2}{calS}({{bf{q},omega)=G(omega),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="380" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img142.gif"
 ALT="\begin{displaymath}
lim_{q\to 0} \frac{\omega^2}{q^2}{\cal S}({\bf q},\omega) =
G(\omega),
\end{displaymath}">|; 

$key = q/{displaymath}S_{AB}(m)=frac{1}{2N_t}sum_{n=0}^{2N_t-1}expleft[2piileft(frac{mn}{rac{n}{2N_t}right)tildeBleft(frac{n}{2N_t}right){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="504" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img660.gif"
 ALT="\begin{displaymath}
S_{AB}(m) = \frac{1}{2N_t}\sum_{n=0}^{2N_t-1}
\exp\left[2\p...
...\left(\frac{n}{2N_t}\right)\tilde B\left(\frac{n}{2N_t}\right)
\end{displaymath}">|; 

$key = q/4times4;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img200.gif"
 ALT="$4\times 4$">|; 

$key = q/{displaymath}PDF(r)=sum_{I=1,JgeI}^{N_{species}}n_In_Jomega_Iomega_Jg_{IJ}(r){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="413" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img536.gif"
 ALT="\begin{displaymath}
PDF(r)=\sum_{I = 1,J\ge I}^{N_{species}}n_In_J \omega_I \omega_J g_{IJ}(r)
\end{displaymath}">|; 

$key = q/{displaymath}{MSD}(n)=2Deltat^2cdotfrac{1}{2pii}ointdzz^{n-1}cdotfrac{z}{(z-1)^2}cdotC_{vv}^{>}(z).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="474" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img293.gif"
 ALT="\begin{displaymath}
{MSD}(n)= 2\Delta t^2\cdot\frac{1}{2\pi i}\oint dz z^{n-1}\cdot\frac{z}{(z-1)^2}\cdot C_{vv}^{&gt;}(z).
\end{displaymath}">|; 

$key = q/{displaymath}q=sqrt{2-frac{hbaromega}{E_0}-2costhetasqrt{2-frac{hbaromega}{E_0}}}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="404" HEIGHT="65" BORDER="0"
 SRC="|."$dir".q|img405.gif"
 ALT="\begin{displaymath}
q = \sqrt{2 - \frac{\hbar\omega}{E_0}
- 2\cos\theta\sqrt{2 - \frac{\hbar\omega}{E_0}}}.
\end{displaymath}">|; 

$key = q/{displaymath}Phi_i(t)=int_0^tdtau,omega'_i(tau).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img386.gif"
 ALT="\begin{displaymath}
\Phi_i(t) = \int_0^t d\tau \omega'_i(\tau).
\end{displaymath}">|; 

$key = q/{displaymath}{{bf{D}({{bf{q})=left(array{{ccc}q_0^2+q_1^2-q^2_2-q^2_3&2(-q_0q_3+q_1+q_2q_3)&q_0^2+q_3^2-q^2_1-q^2_2array{right).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="544" HEIGHT="73" BORDER="0"
 SRC="|."$dir".q|img185.gif"
 ALT="\begin{displaymath}
{\bf D}({\bf q}) =
\left(
\begin{array}{ccc}
q_0^2+q_1^2-q...
...2(q_0q_1+q_2q_3) &amp;q_0^2+q_3^2-q^2_1-q^2_2
\end{array}\right).
\end{displaymath}">|; 

$key = q/{{bf{n}_q;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img149.gif"
 ALT="${\bf n}_q$">|; 

$key = q/{displaymath}rho_0=frac{N}{V}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="309" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img546.gif"
 ALT="\begin{displaymath}
\rho_0 = \frac{N}{V}
\end{displaymath}">|; 

$key = q/{displaymath}mathrm{VACF}^{(AR)}_{>}(z)=sum_{n=0}^{infty}VACF^{(AR)}(n)z^{-n}=sum_{j=1}^{P}beta_jfrac{z_j}{z-z_j},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="487" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img302.gif"
 ALT="\begin{displaymath}
\mathrm{VACF}^{(AR)}_{&gt;}(z)=\sum_{n=0}^{\infty}VACF^{(AR)}(n) z^{-n} = \sum_{j=1}^{P}\beta_j \frac{z_j}{z-z_j},
\end{displaymath}">|; 

$key = q/^{a};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.gif"
 ALT="$^{a}$">|; 

$key = q/displaystyle{{bf{M}{{bf{q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img218.gif"
 ALT="$\displaystyle {\bf M}{\bf q}$">|; 

$key = q/q_{min}:q_{max}:q_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="137" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img464.gif"
 ALT="$q_{min}:q_{max}:q_{step}$">|; 

$key = q/rho_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img545.gif"
 ALT="$\rho_0$">|; 

$key = q/{{bf{r}_{alpha}(t_{ref});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img117.gif"
 ALT="${\bf r}_{\alpha}(t_{ref})$">|; 

$key = q/{displaymath}CN(r_m)doteqfrac{1}{N_{frames}}frac{1}{N_{G}}sum_{f=1}^{N_{frames}}m,t_f),qquadm=0ldotsN_r-1,;n=0ldotsN_{frames}-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="748" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img559.gif"
 ALT="\begin{displaymath}
CN(r_m) \doteq \frac{1}{N_{frames}}\frac{1}{N_{G}}\sum_{f=1}...
..._f),
\qquad m = 0\ldots N_r - 1,\; n = 0\ldots N_{frames} - 1.
\end{displaymath}">|; 

$key = q/{{bf{r}={{bf{0};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img517.gif"
 ALT="${\bf r} = {\bf0}$">|; 

$key = q/displaystylesum_{j=0}^Nxi(j),z^{-j}.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img276.gif"
 ALT="$\displaystyle \sum_{j=0}^N \xi(j)   z^{-j}.$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashgroup_selection_from_a_selection_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img73.gif"
 ALT="\includegraphics[width=10cm]{Figures/group_selection_from_a_selection_file.eps}">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_forward.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img627.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_forward.eps}">|; 

$key = q/i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img14.gif"
 ALT="$i$">|; 

$key = q/[theta,theta+dtheta];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img574.gif"
 ALT="$[\theta , \theta + d\theta]$">|; 

$key = q/(2j+1)times(2j+1);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="149" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img377.gif"
 ALT="$(2j + 1)\times(2j + 1)$">|; 

$key = q/xi(0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img282.gif"
 ALT="$\xi(0)$">|; 

$key = q/{displaymath}sum_{j=0}^infty~xi(j),z^{-j}=frac{1}{Deltat^2},frac{sum_{j=0}^infty)right],z^{-j}}{sum_{j=0}^infty,VACF(j),z^{-j}}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="505" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img270.gif"
 ALT="\begin{displaymath}
\sum_{j=0}^\infty&nbsp; \xi(j)   z^{-j} = \frac{1}{\Delta t^2}...
...ight]   z^{-j}}
{ \sum_{j=0}^\infty   VACF(j)   z^{-j} }.
\end{displaymath}">|; 

$key = q/displaystyleB(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img652.gif"
 ALT="$\displaystyle B(k)$">|; 

$key = q/displaystyle{{bf{X}^{(0)};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img178.gif"
 ALT="$\displaystyle {\bf X}^{(0)}$">|; 

$key = q/{displaymath}partial_t{mathcalF_{inc}}(mathbf{q},t)=-int_{0}^{t}dtau,xi(mathbf{q},t-tau){mathcalF_{inc}}(mathbf{q},tau).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="443" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img487.gif"
 ALT="\begin{displaymath}
\partial_t{\mathcal F_{inc}}(\mathbf{q},t) =
-\int_{0}^{t}d\tau \xi(\mathbf{q},t-\tau){\mathcal F_{inc}}(\mathbf{q},\tau).
\end{displaymath}">|; 

$key = q/r(t_i);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img151.gif"
 ALT="$r(t_i)$">|; 

$key = q/rho_I({{bf{q},kcdotDeltat);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img446.gif"
 ALT="$\rho_I({\bf q},k\cdot\Delta t)$">|; 

$key = q/sigma_P;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img233.gif"
 ALT="$\sigma_P$">|; 

$key = q/omega_{I,mathrm{inc}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img483.gif"
 ALT="$\omega_{I,\mathrm{inc}}$">|; 

$key = q/p^j_{mn}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img351.gif"
 ALT="$p^j_{m n}(t)$">|; 

$key = q/{{bf{r}_{cms}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.gif"
 ALT="${\bf r}_{cms}(t)$">|; 

$key = q/lambda={{bf{q}cdot{{bf{M}{{bf{q}equivm({{bf{q});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="155" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img223.gif"
 ALT="$\lambda = {\bf q}\cdot{\bf M}{\bf q} \equiv m({\bf q})$">|; 

$key = q/{displaymath}C_{ii}(t)=leftlangleP_2(mu_i(0)cdotmu_i(t))rightrangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="379" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img602.gif"
 ALT="\begin{displaymath}
C_{ii}(t) = \left\langle P_2(\mu_i(0) \cdot \mu_i(t)) \right\rangle
\end{displaymath}">|; 

$key = q/sqrt{omega_I};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img460.gif"
 ALT="$\sqrt{\omega_I}$">|; 

$key = q/C^G(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img607.gif"
 ALT="$C^G(t)$">|; 

$key = q/{displaymath}dot{r}(t_i)=frac{r(t_{i+1})-r(t_{i})}{Deltat},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="44" BORDER="0"
 SRC="|."$dir".q|img152.gif"
 ALT="\begin{displaymath}
\dot{r}(t_i)=\frac{r(t_{i+1})-r(t_{i})} {\Delta t},
\end{displaymath}">|; 

$key = q/q_{max};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img470.gif"
 ALT="$q_{max}$">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_move.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img628.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_move.eps}">|; 

$key = q/N_{rvalues}=E(frac{r_{max}-r_{min}}{r_{step}})+1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="221" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img554.gif"
 ALT="$N_{rvalues}= E(\frac{r_{max} - r_{min}}{r_{step}}) + 1$">|; 

$key = q/{displaymath}xi_0:=sum_{n=0}^{infty}Deltat:xi(n)=Deltat,Xi_{>}(1).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="392" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img300.gif"
 ALT="\begin{displaymath}
\xi_0 := \sum_{n=0}^{\infty}\Delta t\:\xi(n) =
\Delta t \Xi_{&gt;}(1).
\end{displaymath}">|; 

$key = q/SUMSQ=2cdotsum_{k=0}^{N_t-1}DSQ(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="238" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img110.gif"
 ALT="$SUMSQ = 2\cdot\sum_{k=0}^{N_t-1} DSQ(k)$">|; 

$key = q/T;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img162.gif"
 ALT="$T$">|; 

$key = q/E_0=hbar^2k_0^2slash2m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img397.gif"
 ALT="$E_0 = \hbar^2 k_0^2/2m$">|; 

$key = q/{{bf{Omega}_alpha(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img355.gif"
 ALT="${\bf\Omega}_\alpha(t)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsscsf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="544" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img535.gif"
 ALT="\includegraphics[width=10cm]{Figures/sscsf.eps}">|; 

$key = q/beta_j;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img252.gif"
 ALT="$\beta_j$">|; 

$key = q/hatT^j_l;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img365.gif"
 ALT="$\hat T^j_l$">|; 

$key = q/deg;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img584.gif"
 ALT="$\deg$">|; 

$key = q/r=x,y,z;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img153.gif"
 ALT="$r=x,y,z$">|; 

$key = q/p({{bf{Omega}_1,t_1;{{bf{Omega}_0,t_0)=p({{bf{Omega}_1,t_1|{{bf{Omega}_0,t_0)cdotp({{bf{Omega}_0,t_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="340" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img344.gif"
 ALT="$p({\bf\Omega}_1,t_1;{\bf\Omega}_0,t_0) = p({\bf\Omega}_1,t_1\vert{\bf\Omega}_0,t_0)\cdot p({\bf\Omega}_0,t_0)$">|; 

$key = q/displaystylefrac{1}{sqrt{n_In_J}}sum^{n_I}_{alpha}sum^{n_J}_{beta}langleexp[-i{{pha(t_0)]exp[i{{bf{q}cdothat{{bf{R}_beta(t_0+t)]rangle_{t_0},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="423" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img429.gif"
 ALT="$\displaystyle \frac{1}{\sqrt{n_In_J}}\sum^{n_I}_{\alpha}\sum^{n_J}_{\beta}
\lan...
...t{\bf R}_\alpha(t_0)]
\exp[i{\bf q}\cdot\hat{\bf R}_\beta(t_0+t)]\rangle_{t_0},$">|; 

$key = q/{displaymath}sum^{N_{species}}_Isqrt{n_Iomega_I}sum^{n_I}_{alpha=1}exp[-i{{bf{q}cdot{{bf{R}_alpha(nDeltat)]{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="422" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img479.gif"
 ALT="\begin{displaymath}
\sum^{N_{species}}_I \sqrt{n_I \omega_I} \sum^{n_I}_{\alpha=1} \exp[-i{\bf q}\cdot{\bf R}_\alpha(n\Delta t)]
\end{displaymath}">|; 

$key = q/r_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img552.gif"
 ALT="$r_{step}$">|; 

$key = q/^{15};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img600.gif"
 ALT="$^{15}$">|; 

$key = q/textstyle=;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img46.gif"
 ALT="$\textstyle =$">|; 

$key = q/b(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img643.gif"
 ALT="$b(k)$">|; 

$key = q/{displaymath}Delta^2(m)=frac{1}{N_t-m}sum_{k=0}^{N_t-m-1}[textbf{r}(k+m)-textbf{r}(k)]^2,qquadm=0ldotsN_t-1,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="531" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img94.gif"
 ALT="\begin{displaymath}
\Delta^2(m) = \frac{1}{N_t - m}\sum_{k=0}^{N_t-m-1}
[\textbf{r}(k+m) - \textbf{r}(k)]^2, \qquad m = 0\ldots N_t-1,
\end{displaymath}">|; 

$key = q/alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img39.gif"
 ALT="$\alpha$">|; 

$key = q/omega_J;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img537.gif"
 ALT="$\omega_J$">|; 

$key = q/n_I;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.gif"
 ALT="$n_I$">|; 

$key = q/alpha,beta,gamma;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img368.gif"
 ALT="$\alpha,\beta,\gamma$">|; 

$key = q/{displaymath}MSD(n)=2sum_{k=0}^nDeltat^2(n-k)C_{vv}(k).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="415" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img287.gif"
 ALT="\begin{displaymath}
MSD(n)=2 \sum_{k=0}^n \Delta t^2(n-k)C_{vv}(k).
\end{displaymath}">|; 

$key = q/displaystyletildeBleft(frac{n}{2N_t}right);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img665.gif"
 ALT="$\displaystyle \tilde B\left(\frac{n}{2N_t}\right)$">|; 

$key = q/xi(j);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img273.gif"
 ALT="$\xi(j)$">|; 

$key = q/N_r;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img560.gif"
 ALT="$N_r$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsd.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="606" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img583.gif"
 ALT="\includegraphics[width=10cm]{Figures/sd.eps}">|; 

$key = q/displaystyle{calF}_{mathrm{inc}}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img426.gif"
 ALT="$\displaystyle {\cal F}_{\mathrm{inc}}({\bf q},t)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdisf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="744" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img486.gif"
 ALT="\includegraphics[width=10cm]{Figures/disf.eps}">|; 

$key = q/F_{>}(z)=sum_{n=0}^{infty}f(n)z^{-n};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img266.gif"
 ALT="$F_{&gt;}(z) = \sum_{n=0}^{\infty} f(n) z^{-n}$">|; 

$key = q/{displaymath}frac{d}{dt}VACF(t)=-int_{0}^{t}dtau,xi(t-tau)VACF(tau).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="590" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img244.gif"
 ALT="\begin{displaymath}
\frac{d}{dt}\VACF(t) = -\int_{0}^{t}d\tau  \xi(t-\tau)\VACF(\tau).
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashconvert_netcdf_ascii.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="405" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img24.gif"
 ALT="\includegraphics[width=10cm]{Figures/convert_netcdf_ascii.eps}">|; 

$key = q/displaystyleoverline{b_alpha},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img417.gif"
 ALT="$\displaystyle \overline{b_\alpha},$">|; 

$key = q/displaystylesum_{alpha}b^2_{alpha,inc}tildeC_{vv;alphaalpha}(omega),;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="150" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img145.gif"
 ALT="$\displaystyle \sum_{\alpha}b^2_{\alpha,inc}
\tilde C_{vv ; \alpha\alpha}(\omega),$">|; 

$key = q/@;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img9.gif"
 ALT="$@$">|; 

$key = q/{displaymath}omega=left(k_{B}Tslashlambda^{prime}right)^{1slash2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="343" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img321.gif"
 ALT="\begin{displaymath}
\omega = \left (k_{B}T/\lambda^{\prime} \right )^{1/2}
\end{displaymath}">|; 

$key = q/F_{I,mathrm{inc}}(q_m,kcdotDeltat);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="131" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img484.gif"
 ALT="$F_{I,\mathrm{inc}}(q_m,k\cdot\Delta t)$">|; 

$key = q/{displaymath}b_{mathrm{inc}}^2G_s({{bf{r},t)doteqfrac{1}{2pi^3}intd^3q,exp[-i{{bf{q}cdot{{bf{r}]{calF}_{inc}({{bf{q},t),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="456" HEIGHT="43" BORDER="0"
 SRC="|."$dir".q|img516.gif"
 ALT="\begin{displaymath}
b_{\mathrm{inc}}^2 G_s({\bf r},t) \doteq \frac{1}{2\pi^3}
\int d^3q \exp[-i{\bf q}\cdot{\bf r}] {\cal F}_{inc}({\bf q},t),
\end{displaymath}">|; 

$key = q/{{bf{r}_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img214.gif"
 ALT="${\bf r}_\alpha$">|; 

$key = q/displaystylesum_alphaomega_alpha,{{bf{x}_alpha,;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img177.gif"
 ALT="$\displaystyle \sum_\alpha \omega_\alpha {\bf x}_\alpha,$">|; 

$key = q/V;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img547.gif"
 ALT="$V$">|; 

$key = q/displaystyleb_{alpha,mathrm{coh}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img45.gif"
 ALT="$\displaystyle b_{\alpha,\mathrm{coh}}$">|; 

$key = q/{displaymath}EISF_I(q_m)=frac{1}{n_I}sum^{n_I}_{alpha}overline{langle|exp[i{{bf{q}cdot{{bf{R}_alpha]|^2rangle}^q.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="428" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img525.gif"
 ALT="\begin{displaymath}
EISF_I(q_m) = \frac{1}{n_I}\sum^{n_I}_{\alpha} \overline{ \langle\vert\exp[i{\bf q}\cdot{\bf R}_\alpha]\vert^2\rangle}^q.
\end{displaymath}">|; 

$key = q/{displaymath}sum_{alpha=1}^Nb_{alpha,mathrm{inc}}exp[-i{{bf{q}cdot{{bf{R}_alpha(t)].{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img489.gif"
 ALT="\begin{displaymath}
\sum_{\alpha=1}^N b_{\alpha,\mathrm{inc}}\exp[-i{\bf q}\cdot{\bf R}_\alpha(t)].
\end{displaymath}">|; 

$key = q/{displaymath}RDF(r)=4pir^2rho_0PDF(r){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="381" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img543.gif"
 ALT="\begin{displaymath}
RDF(r) = 4 \pi r^2 \rho_0 PDF(r)
\end{displaymath}">|; 

$key = q/{displaymath}array{{ll}&array{{ll}SUMSQ&leftarrowSUMSQ-DSQ(m-1)-DSQ(N_t-m)MSD(m)rray{&m;mbox{runningfrom};0;mbox{to};N_t-1array{{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="505" HEIGHT="72" BORDER="0"
 SRC="|."$dir".q|img111.gif"
 ALT="\begin{displaymath}
\begin{array}{ll}
&amp;\begin{array}{ll}
SUMSQ &amp;\leftarrow SU...
... \mbox{running from}\; 0 \;\mbox{to}\; N_t - 1\\\\
\end{array} \end{displaymath}">|; 

$key = q/includegraphics[width=5.2cm]{Figuresslashpreferences_external_programs.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="235" HEIGHT="198" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img29.gif"
 ALT="\includegraphics[width=5.2cm]{Figures/preferences_external_programs.eps}">|; 

$key = q/N_{rvalues};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img553.gif"
 ALT="$N_{rvalues}$">|; 

$key = q/{displaymath}c_{ab}(m)=frac{1}{N_t-|m|}S_{AB}(m),qquad-(N_t-1)lemleN_t-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="500" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img659.gif"
 ALT="\begin{displaymath}
c_{ab}(m) = \frac{1}{N_t-\vert m\vert}S_{AB}(m),\qquad -(N_t-1) \le m \le N_t-1.
\end{displaymath}">|; 

$key = q/f(n);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img248.gif"
 ALT="$f(n)$">|; 

$key = q/t=NDeltat;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img271.gif"
 ALT="$t=N\Delta t$">|; 

$key = q/nm^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img113.gif"
 ALT="$nm^2$">|; 

$key = q/NslashV;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img450.gif"
 ALT="$N/V$">|; 

$key = q/displaystylefrac{1}{N}left[overline{mbox{{b_alpha{}},overline{mbox{{b_beta{}}+dehabeta}(overline{b_alpha^{,2}}-overline{b_alpha}^{,2})right].;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="214" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img411.gif"
 ALT="$\displaystyle \frac{1}{N}
\left[\overline{\mbox{$b_\alpha$}} \overline{\mbox{$...
...a_{\alpha\beta}
(\overline{b_\alpha^{ 2}} - \overline{b_\alpha}^{ 2})\right].$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashplot.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="452" HEIGHT="590" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img622.gif"
 ALT="\includegraphics[width=10cm]{Figures/plot.eps}">|; 

$key = q/N_{atoms};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img40.gif"
 ALT="$N_{atoms}$">|; 

$key = q/{displaymath}RMSD(t)=sqrt{frac{sum_{alpha=1}^{N_{alpha}}({{bf{r}_{alpha}(t)-{{bf{r}_{alpha}(t_{ref}))}{N_{alpha}}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="430" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img114.gif"
 ALT="\begin{displaymath}
RMSD(t) = \sqrt{\frac{\sum_{\alpha = 1}^{N_{\alpha}}({\bf r}_{\alpha}(t) - {\bf r}_{\alpha}(t_{ref}))}{N_{\alpha}}}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashavacf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="561" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img387.gif"
 ALT="\includegraphics[width=10cm]{Figures/avacf.eps}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashexport_plot.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="242" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img624.gif"
 ALT="\includegraphics[width=10cm]{Figures/export_plot.eps}">|; 

$key = q/{{bf{Omega}_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img340.gif"
 ALT="${\bf\Omega}_0$">|; 

$key = q/^{c};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img3.gif"
 ALT="$^{c}$">|; 

$key = q/{displaymath}sum_{alpha=1}^Nb_{alpha,mathrm{coh}}exp[-i{{bf{q}cdot{{bf{R}_alpha(t)].{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="380" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img478.gif"
 ALT="\begin{displaymath}
\sum_{\alpha=1}^N b_{\alpha,\mathrm{coh}}\exp[-i{\bf q}\cdot{\bf R}_\alpha(t)].
\end{displaymath}">|; 

$key = q/textbf{n}_q;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img500.gif"
 ALT="$\textbf{n}_q$">|; 

$key = q/displaystylem({{bf{q});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img209.gif"
 ALT="$\displaystyle m({\bf q})$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashvacf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="624" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img150.gif"
 ALT="\includegraphics[width=10cm]{Figures/vacf.eps}">|; 

$key = q/({{bf{Omega},{{bf{Omega}_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img363.gif"
 ALT="$({\bf\Omega},{\bf\Omega}_0)$">|; 

$key = q/langleomegarangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img440.gif"
 ALT="$\langle\omega\rangle$">|; 

$key = q/r_m=r_{min}+mcdotr_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="166" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img550.gif"
 ALT="$r_m = r_{min} + m \cdot r_{step}$">|; 

$key = q/{displaymath}Y^j_m(alpha,beta)=sqrt{frac{2j+1}{4pi}}D^j_{m0}(alpha,beta,gamma),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="408" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img367.gif"
 ALT="\begin{displaymath}
Y^j_m(\alpha,\beta) = \sqrt{\frac{2j+1}{4\pi}}
D^j_{m 0}(\alpha,\beta,\gamma),
\end{displaymath}">|; 

$key = q/{{bf{v}_1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img127.gif"
 ALT="${\bf v}_1$">|; 

$key = q/displaystylefrac{1}{n_I}sum^{n_I}_{alpha}expleft[-frac{(q_m)^2}{6}Delta^2_alpha(kcdotDeltat)right]qquadmbox{{{rm{isotropicsystem}},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="410" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img504.gif"
 ALT="$\displaystyle \frac{1}{n_I}\sum^{n_I}_{\alpha} \exp\left[-\frac{(q_m)^2}{6}
\Delta^2_\alpha(k\cdot\Delta t)\right]
\qquad\mbox{{\rm isotropic system}},$">|; 

$key = q/T^j_m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img359.gif"
 ALT="$T^j_m$">|; 

$key = q/hbarq;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img442.gif"
 ALT="$\hbar q$">|; 

$key = q/displaystylefrac{1}{N}sum_{alpha}b_{alpha,inc}^{,2}langleexp[-i{{bf{q}cdothat{{bf{R}_alpha(0)]exp[i{{bf{q}cdothat{{bf{R}_alpha(t)]rangle.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="351" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img423.gif"
 ALT="$\displaystyle \frac{1}{N}\sum_{\alpha}
b_{\alpha,inc}^{ 2}
\langle\exp[-i{\bf q}\cdot\hat{\bf R}_\alpha(0)]
\exp[i{\bf q}\cdot\hat{\bf R}_\alpha(t)]\rangle.$">|; 

$key = q/{{bf{x}^{(0)}_alpha,,alpha=1ldotsN;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="131" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img168.gif"
 ALT="${\bf x}^{(0)}_\alpha, \alpha = 1\ldots N$">|; 

$key = q/{displaymath}p({{bf{Omega}_1,t_1;{{bf{Omega}_0,t_0)=p({{bf{Omega},t|{{bf{0},0)cdotfrac{1}{8pi^2}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="415" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img361.gif"
 ALT="\begin{displaymath}
p({\bf\Omega}_1,t_1;{\bf\Omega}_0,t_0) =
p({\bf\Omega},t\vert{\bf0},0)\cdot\frac{1}{8\pi^2}.
\end{displaymath}">|; 

$key = q/N_t;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.gif"
 ALT="$N_t$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashforcite_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img20.gif"
 ALT="\includegraphics[width=10cm]{Figures/forcite_converter.eps}">|; 

$key = q/N_{theta}=E(frac{theta_{max}-theta_{min}}{theta_{step}})+1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img589.gif"
 ALT="$N_{\theta}= E(\frac{\theta_{max} - \theta_{min}}{\theta_{step}}) + 1$">|; 

$key = q/[r_m,r_m+dr];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img562.gif"
 ALT="$[r_m,r_m + dr]$">|; 

$key = q/displaystyle{{bf{x}^{(0)}_alpha-{{bf{X}^{(0)},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="94" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img184.gif"
 ALT="$\displaystyle {\bf x}^{(0)}_\alpha - {\bf X}^{(0)},$">|; 

$key = q/{displaymath}langleomega^{2n+1}rangledoteqint_{-infty}^{+infty}domega,omega^{2n+1}{calS}({{bf{q},omega),qquadn=1,2,ldots.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="472" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img437.gif"
 ALT="\begin{displaymath}
\langle\omega^{2n+1}\rangle \doteq
\int_{-\infty}^{+\infty}d...
...\omega^{2n+1} {\cal S}({\bf q},\omega), \qquad n = 1,2,\ldots.
\end{displaymath}">|; 

$key = q/{displaymath}C^I_{ii}(t)=S^2_i+(1-S^2_i)expleft(-frac{t}{tau_{eff,i}}right){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="418" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img609.gif"
 ALT="\begin{displaymath}
C^I_{ii}(t) = S^2_i +(1 - S^2_i)exp\left(-\frac{t}{\tau_{eff,i}}\right)
\end{displaymath}">|; 

$key = q/displaystylesum_{k=0}^{2N_t-1}expleft[-2piileft(frac{nk}{2N_t}right)right],A(k),;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="240" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img664.gif"
 ALT="$\displaystyle \sum_{k=0}^{2N_t-1} \exp\left[-2\pi i\left(\frac{n k}{2N_t}\right)\right]
 A(k),$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashvasp_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img22.gif"
 ALT="\includegraphics[width=10cm]{Figures/vasp_converter.eps}">|; 

$key = q/{displaymath}xi^{(AR)}_0=frac{1}{Deltat}frac{1}{sum_{j=1}^{P}beta_jfrac{1}{1-z_j}}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="375" HEIGHT="53" BORDER="0"
 SRC="|."$dir".q|img306.gif"
 ALT="\begin{displaymath}
\xi^{(AR)}_0 = \frac{1}{\Delta t}
\frac{1}{\sum_{j=1}^{P}\beta_j\frac{1}{1-z_j}}.
\end{displaymath}">|; 

$key = q/{displaymath}sqrt{{calW_I}}=frac{n_{I}b_{I,mathrm{coh}}}{sqrt{sum^{N_{species}}_{I=1}b_{I,mathrm{coh}}^2}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img58.gif"
 ALT="\begin{displaymath}
\sqrt{{\cal W_I}}=\frac{n_{I}b_{I,\mathrm{coh}}}{\sqrt{\sum^{N_{species}}_{I=1} b_{I,\mathrm{coh}}^2}}
\end{displaymath}">|; 

$key = q/{displaymath}{{bf{q}=2pileft(k{{bf{b}^1+l{{bf{b}^2+m{{bf{b}^3right),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="387" HEIGHT="37" BORDER="0"
 SRC="|."$dir".q|img458.gif"
 ALT="\begin{displaymath}
{\bf q} = 2\pi\left(k{\bf b}^1 + l{\bf b}^2 + m{\bf b}^3\right),
\end{displaymath}">|; 

$key = q/displaystylesum_{k=0}^{2N_t-1}expleft[-2piileft(frac{nk}{2N_t}right)right],B(k).;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="241" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img666.gif"
 ALT="$\displaystyle \sum_{k=0}^{2N_t-1} \exp\left[-2\pi i\left(\frac{n k}{2N_t}\right)\right]
 B(k).$">|; 

$key = q/omega_{alpha,I};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img60.gif"
 ALT="$\omega_{\alpha,I}$">|; 

$key = q/{displaymath}sqrt{omega_I}=frac{b_{I,mathrm{coh}}}{sqrt{sum_{I=1}^{N_{species}}n_Ib^2_{I,mathrm{coh}}}}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="384" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img461.gif"
 ALT="\begin{displaymath}
\sqrt{\omega_I} = \frac{b_{I,\mathrm{coh}}}{\sqrt{\sum_{I = 1}^{N_{species}} n_I b^2_{I,\mathrm{coh}}}}.
\end{displaymath}">|; 

$key = q/^{1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img601.gif"
 ALT="$^{1}$">|; 

$key = q/sigma_nu;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img671.gif"
 ALT="$\sigma_\nu$">|; 

$key = q/{displaymath}{MSD}(t)=langle[x(t)-x(0)]^2rangle=2int_{0}^{t}dtau,(t-tau)C_{vv}(t){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="470" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img286.gif"
 ALT="\begin{displaymath}
{MSD}(t)=\langle[x(t)-x(0)]^2\rangle=2 \int_{0}^{t}d\tau (t - \tau)
C_{vv}(t)
\end{displaymath}">|; 

$key = q/displaystyle{calF}_{I,mathrm{inc}}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img430.gif"
 ALT="$\displaystyle {\cal F}_{I,\mathrm{inc}}({\bf q},t)$">|; 

$key = q/{displaymath}{calF}^{g}_{mathrm{inc}}(textbf{q},t)=sum^{N_{species}}_{I=1}n_Iomehrm{inc}}{calF}^{g}_{I,mathrm{inc}}(textbf{q},t){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="421" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img492.gif"
 ALT="\begin{displaymath}
{\cal F}^{g}_{\mathrm{inc}}(\textbf{q},t) = \sum^{N_{species...
...a_{I,\mathrm{inc}} {\cal F}^{g}_{I,\mathrm{inc}}(\textbf{q},t)
\end{displaymath}">|; 

$key = q/Deltax_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img326.gif"
 ALT="$\Delta x_i$">|; 

$key = q/{calR}^t_ii=1,2ldotsN_{triplets};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img578.gif"
 ALT="${\cal R}^t_i i = 1, 2 \ldots N_{triplets}$">|; 

$key = q/r_{max};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img555.gif"
 ALT="$r_{max}$">|; 

$key = q/{displaymath}{calW_I}=frac{n_{I}m_{I}}{sum^{N_{species}}_{I=1}n_{I}m_{I}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="363" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img55.gif"
 ALT="\begin{displaymath}
{\cal W_I}=\frac{n_{I}m_{I}}{\sum^{N_{species}}_{I=1} n_{I}m_{I}}
\end{displaymath}">|; 

$key = q/N_{modes};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img334.gif"
 ALT="$N_{modes}$">|; 

$key = q/{displaymath}c_{ab}(m)=c^*_{ba}(-m).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="351" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img648.gif"
 ALT="\begin{displaymath}
c_{ab}(m) = c^*_{ba}(-m).
\end{displaymath}">|; 

$key = q/q_m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img524.gif"
 ALT="$q_m$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashados.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="601" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img388.gif"
 ALT="\includegraphics[width=10cm]{Figures/ados.eps}">|; 

$key = q/{{bf{I};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img187.gif"
 ALT="${\bf I}$">|; 

$key = q/m=n=0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img379.gif"
 ALT="$m = n = 0$">|; 

$key = q/displaystyleS_{AA+BB}(0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="93" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.gif"
 ALT="$\displaystyle S_{AA+BB}(0)$">|; 

$key = q/{displaymath}n(r,r+dr)=frac{1}{N_{G}}sum_{g=1}^{N_{G}}sum_{I=1}^{N_{species}}n_{gI}(r,r+dr){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="441" HEIGHT="63" BORDER="0"
 SRC="|."$dir".q|img556.gif"
 ALT="\begin{displaymath}
n(r, r+dr) = \frac{1}{N_{G}}\sum_{g=1}^{N_{G}}\sum_{I=1}^{N_{species}} n_{gI}(r, r+dr)
\end{displaymath}">|; 

$key = q/{a^{(P)}_n};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img234.gif"
 ALT="$\{a^{(P)}_n\}$">|; 

$key = q/displaystyle{{bf{q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img400.gif"
 ALT="$\displaystyle {\bf q}$">|; 

$key = q/includegraphics[width=5.2cm]{Figuresslashpreferences_miscellaneous.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="235" HEIGHT="198" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img30.gif"
 ALT="\includegraphics[width=5.2cm]{Figures/preferences_miscellaneous.eps}">|; 

$key = q/<r^2>;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.gif"
 ALT="$&lt;r^2&gt;$">|; 

$key = q/displaystylelambda{{bf{q},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img219.gif"
 ALT="$\displaystyle \lambda{\bf q},$">|; 

$key = q/{displaymath}S^2_i(t)=tanhleft(2.656sum_kleft((exp(-r^O_{i-1,k}(t))))+0.8exp(-r^H_{i,k}(t))right)right)+b{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="541" HEIGHT="56" BORDER="0"
 SRC="|."$dir".q|img616.gif"
 ALT="\begin{displaymath}
S^2_i(t) = tanh\left(2.656\sum_k \left((exp(-r^O_{i-1,k}(t)))) + 0.8exp(-r^H_{i,k}(t))\right)\right) + b
\end{displaymath}">|; 

$key = q/{displaymath}EISF({{bf{q})=b_{mathrm{inc}}^2intd^3r,exp[i{{bf{q}cdot{{bf{r}],G_s({{bf{r},t=infty).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="456" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img518.gif"
 ALT="\begin{displaymath}
EISF({\bf q}) = b_{\mathrm{inc}}^2\int d^3r \exp[i{\bf q}\cdot{\bf r}] 
G_s({\bf r},t=\infty).
\end{displaymath}">|; 

$key = q/N_{Frames};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img227.gif"
 ALT="$N_{Frames}$">|; 

$key = q/dOmega;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img392.gif"
 ALT="$d\Omega$">|; 

$key = q/xi(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img229.gif"
 ALT="$\xi(t)$">|; 

$key = q/textbf{r}(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img95.gif"
 ALT="$\textbf{r}(k)$">|; 

$key = q/2N_t;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img654.gif"
 ALT="$2N_t$">|; 

$key = q/{displaymath}|z_j|<1,quadj=1,ldots,P,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="369" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img255.gif"
 ALT="\begin{displaymath}
\vert z_j\vert &lt; 1,\quad j=1,\ldots,P,
\end{displaymath}">|; 

$key = q/|{{bf{A}|=|{{bf{A}{{bf{Q}|;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img208.gif"
 ALT="$\Vert{\bf A}\Vert = \Vert{\bf A}{\bf Q}\Vert$">|; 

$key = q/{displaymath}Z_{>}left{f(n+1)-f(n)right}=zF_{>}(z)-zf(0){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="431" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img263.gif"
 ALT="\begin{displaymath}
Z_{&gt;}\left\{f(n+1)-f(n)\right\} =
zF_{&gt;}(z) - zf(0)
\end{displaymath}">|; 

$key = q/b_{coh,H};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img462.gif"
 ALT="$b_{coh,H}$">|; 

$key = q/displaystylez^{-N}left(sum_{j=0}^N,c_j,z^{N-j}+Rright);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="189" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img275.gif"
 ALT="$\displaystyle z^{-N} \left(\sum_{j=0}^N   c_j   z^{N-j} + R \right)$">|; 

$key = q/{displaymath}C_{ii}(t)=C^G(t)cdotC^I_{ii}(t){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="362" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img606.gif"
 ALT="\begin{displaymath}
C_{ii}(t) = C^G(t) \cdot C^I_{ii}(t)
\end{displaymath}">|; 

$key = q/b_{alpha,mathrm{inc}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img43.gif"
 ALT="$b_{\alpha,\mathrm{inc}}$">|; 

$key = q/{calF}_{mathrm{inc}}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img481.gif"
 ALT="${\cal F}_{\mathrm{inc}}({\bf q},t)$">|; 

$key = q/z=exp(iomegaDeltat);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img257.gif"
 ALT="$z=\exp(i\omega\Delta t)$">|; 

$key = q/p(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img308.gif"
 ALT="$p(z)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashmsd.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="544" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img112.gif"
 ALT="\includegraphics[width=10cm]{Figures/msd.eps}">|; 

$key = q/c^g_iqquadi=1,2ldotsN_{groups};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="193" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img577.gif"
 ALT="$c^g_i \qquad i = 1, 2 \ldots N_{groups}$">|; 

$key = q/displaystyletildeC_{vv;alphaalpha}(omega);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img146.gif"
 ALT="$\displaystyle \tilde C_{vv ; \alpha\alpha}(\omega)$">|; 

$key = q/n_{gI}(r);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img558.gif"
 ALT="$n_{gI}(r)$">|; 

$key = q/displaystyletildeAleft(frac{n}{2N_t}right);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img663.gif"
 ALT="$\displaystyle \tilde A\left(\frac{n}{2N_t}\right)$">|; 

$key = q/N_{phi}=E(frac{phi_{max}-phi_{min}}{phi_{step}})+1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="188" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img595.gif"
 ALT="$N_{\phi}= E(\frac{\phi_{max} - \phi_{min}}{\phi_{step}}) + 1$">|; 

$key = q/{displaymath}{calW_I}=frac{n_I}{N_{atoms}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="332" HEIGHT="41" BORDER="0"
 SRC="|."$dir".q|img54.gif"
 ALT="\begin{displaymath}
{\cal W_I}=\frac{n_I}{N_{atoms}}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashmsd_autostart_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="314" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img636.gif"
 ALT="\includegraphics[width=10cm]{Figures/msd_autostart_file.eps}">|; 

$key = q/displaystyleS_{AB}(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img102.gif"
 ALT="$\displaystyle S_{AB}(m)$">|; 

$key = q/p^2_{00}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img381.gif"
 ALT="$p^2_{0 0}(t)$">|; 

$key = q/{displaymath}m({{bf{q})=sum_alphaomega_alpha|{{bf{Q}{{bf{R}^{(0)}_alpha{{bf{Q}^T-{{bf{R}_alpha|^2stackrel{!}{=}Min.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="438" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img206.gif"
 ALT="\begin{displaymath}
m({\bf q}) = \sum_\alpha \omega_\alpha
\Vert{\bf Q}{\bf R}^...
...}_\alpha{\bf Q}^T - {\bf R}_\alpha\Vert^2
\stackrel{!}{=} Min.
\end{displaymath}">|; 

$key = q/rho_{alpha,k}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img494.gif"
 ALT="$\rho_{\alpha,k}(t)$">|; 

$key = q/{displaymath}W(m)=expleft[-frac{1}{2}left(alphafrac{|m|}{N_t-1}right)^2right],qquadm=-(N_t-1)ldotsN_t-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="522" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img669.gif"
 ALT="\begin{displaymath}
W(m) = \exp\left[
-\frac{1}{2}\left(\alpha\frac{\vert m\vert}{N_t-1}\right)^2
\right],
\qquad m = -(N_t-1)\ldots N_t-1.
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashfile_browser.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="314" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img12.gif"
 ALT="\includegraphics[width=10cm]{Figures/file_browser.eps}">|; 

$key = q/p({{bf{Omega}_0,t_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img345.gif"
 ALT="$p({\bf\Omega}_0,t_0)$">|; 

$key = q/{displaymath}VACF^{(AR)}(n)=sum_{j=1}^{P}beta_jz_j^{|n|},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="384" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img251.gif"
 ALT="\begin{displaymath}
VACF^{(AR)}(n) = \sum_{j=1}^{P}\beta_jz_j^{\vert n\vert},
\end{displaymath}">|; 

$key = q/{displaymath}|{{bf{Q}|^2doteqq_0^2+q_1^2+q_2^2+q_3^2=frac{1}{4}tr{{{bf{Q}^T{{bf{Q}}=1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="448" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img198.gif"
 ALT="\begin{displaymath}
\Vert{\bf Q}\Vert^2 \doteq
q_0^2 + q_1^2 + q_2^2 + q_3^2 = \frac{1}{4}tr\{{\bf Q}^T{\bf Q}\} = 1.
\end{displaymath}">|; 

$key = q/DSQ(k)=textbf{r}^2(k),qquadk=0ldotsN_t-1;qquadDSQ(-1)=DSQ(N_t)=0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="540" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img109.gif"
 ALT="$DSQ(k) = \textbf{r}^2(k),\qquad k = 0\ldots N_t-1;\qquad
DSQ(-1) = DSQ(N_t) = 0$">|; 

$key = q/{displaymath}F_1^{>}=frac{z}{(z-1)^2}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="338" HEIGHT="43" BORDER="0"
 SRC="|."$dir".q|img291.gif"
 ALT="\begin{displaymath}
F_1^{&gt;}=\frac{z}{(z-1)^2}.
\end{displaymath}">|; 

$key = q/t_0+t;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img86.gif"
 ALT="$t_0+t$">|; 

$key = q/{{bf{v}_3;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img129.gif"
 ALT="${\bf v}_3$">|; 

$key = q/epsilon_P(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img232.gif"
 ALT="$\epsilon_P(t)$">|; 

$key = q/1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img454.gif"
 ALT="$1$">|; 

$key = q/r_{min}:r_{max}:r_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="137" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img549.gif"
 ALT="$r_{min}:r_{max}:r_{step}$">|; 

$key = q/{displaymath}C_{vv;alphaalpha}(t)doteqfrac{1}{3}langle{{bf{v}_alpha(t_0)cdot{{bf{v}_alpha(t_0+t)rangle_{t_0}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="415" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img138.gif"
 ALT="\begin{displaymath}
C_{vv ; \alpha\alpha}(t) \doteq
\frac{1}{3}\langle {\bf v}_\alpha(t_0)\cdot{\bf v}_\alpha(t_0+t)\rangle_{t_0}.
\end{displaymath}">|; 

$key = q/xi(j)=c_{N-j};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img281.gif"
 ALT="$\xi(j) = c_{N-j}$">|; 

$key = q/v(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img242.gif"
 ALT="$v(t)$">|; 

$key = q/mu_i(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img603.gif"
 ALT="$\mu_i(t)$">|; 

$key = q/8pi^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img349.gif"
 ALT="$8\pi^2$">|; 

$key = q/{displaymath}C^I(t)=sum_{i=1}^{N_{bonds}}C^I_{ii}(t){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img613.gif"
 ALT="\begin{displaymath}
C^I(t) = \sum_{i = 1}^{N_{bonds}} C^I_{ii}(t)
\end{displaymath}">|; 

$key = q/r_l;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img566.gif"
 ALT="$r_l$">|; 

$key = q/>;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img31.gif"
 ALT="$&gt;$">|; 

$key = q/proptoN_t^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img638.gif"
 ALT="$\propto N_t^2$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashgroup_selection.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img72.gif"
 ALT="\includegraphics[width=10cm]{Figures/group_selection.eps}">|; 

$key = q/VACF^{(AR)}(n);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img254.gif"
 ALT="$VACF^{(AR)}(n)$">|; 

$key = q/m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img398.gif"
 ALT="$m$">|; 

$key = q/{displaymath}{calS}({{bf{q},omega)approxexp[frac{betahbaromega}{2}]{calS}_{cl}({{bf{q},omega).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="390" HEIGHT="43" BORDER="0"
 SRC="|."$dir".q|img441.gif"
 ALT="\begin{displaymath}
{\cal S}({\bf q},\omega) \approx
\exp[\frac{\beta\hbar\omega}{2}]{\cal S}_{cl}({\bf q},\omega).
\end{displaymath}">|; 

$key = q/{displaymath}langleexp[-i{{bf{q}cdot{{bf{R}_alpha(0)]exp[i{{bf{q}cdot{{bf{R}_alpe=langle|exp[i{{bf{q}cdot{{bf{R}_alpha]|^2rangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="479" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img522.gif"
 ALT="\begin{displaymath}
\langle\exp[-i{\bf q}\cdot{\bf R}_\alpha(0)]
\exp[i{\bf q}\c...
...
\langle\vert\exp[i{\bf q}\cdot{\bf R}_\alpha]\vert^2 \rangle
\end{displaymath}">|; 

$key = q/displaystyle{{bf{M};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img211.gif"
 ALT="$\displaystyle {\bf M}$">|; 

$key = q/Xi^{(AR)}_{>}(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img304.gif"
 ALT="$\Xi^{(AR)}_{&gt;}(z)$">|; 

$key = q/overline{rule{0pt}{5pt}ldots};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img414.gif"
 ALT="$\overline{\rule{0pt}{5pt}\ldots}$">|; 

$key = q/3N^{-1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img329.gif"
 ALT="$3N^{-1}$">|; 

$key = q/k_0equiv|{{bf{k}_0|;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img395.gif"
 ALT="$k_0\equiv \vert{\bf k}_0\vert$">|; 

$key = q/{displaymath}{calS}({{bf{q},omega)=frac{1}{2pi}int_{-infty}^{+infty}dt,exp[-iomegat]{calF}({{bf{q},t).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="429" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img406.gif"
 ALT="\begin{displaymath}
{\cal S}({\bf q},\omega) = \frac{1}{2\pi}\int_{-\infty}^{+\infty}dt  \exp[-i\omega t]{\cal F}({\bf q},t).
\end{displaymath}">|; 

$key = q/{displaymath}{calF}_{mathrm{inc}}(q_m,kcdotDeltat)doteqsum_{I=1}^{N_{species}}n_,kcdotDeltat),qquadk=0ldotsN_t-1,;m=0ldotsN_q-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="647" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img482.gif"
 ALT="\begin{displaymath}
{\cal F}_{\mathrm{inc}}(q_m,k\cdot\Delta t) \doteq \sum_{I =...
...\Delta t),
\qquad k = 0\ldots N_t - 1,\; m = 0\ldots N_q - 1.
\end{displaymath}">|; 

$key = q/{{bf{x}_alpha,,alpha=1ldotsN;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="122" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img167.gif"
 ALT="${\bf x}_\alpha, \alpha = 1\ldots N$">|; 

$key = q/displaystylefrac{1}{4!}left[langled_alpha^4(t;textbf{n}_q)rangle-3langled^2_alpha(t;textbf{n}_q)rangle^2right];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="235" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img498.gif"
 ALT="$\displaystyle \frac{1}{4!}\left[
\langle d_\alpha^4(t;\textbf{n}_q)\rangle - 3\langle d^2_\alpha(t;\textbf{n}_q)
\rangle^2\right]$">|; 

$key = q/DOS(ncdotDeltanu);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img159.gif"
 ALT="$DOS(n\cdot\Delta\nu)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdeuteration_selection_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="281" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img69.gif"
 ALT="\includegraphics[width=10cm]{Figures/deuteration_selection_file.eps}">|; 

$key = q/langlexrangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img337.gif"
 ALT="$\langle x\rangle$">|; 

$key = q/{displaymath}Delta^2_alpha(t,t_0;textbf{n})doteqleftlangled^2_alpha(t,tau;textbf{n})rightrangle_{t_0}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="386" HEIGHT="39" BORDER="0"
 SRC="|."$dir".q|img87.gif"
 ALT="\begin{displaymath}
\Delta^2_\alpha(t,t_0;\textbf{n}) \doteq \left\langle
d^2_\alpha(t,\tau;\textbf{n})\right\rangle_{t_0}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashconvert_ascii_netcdf_example.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="281" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img26.gif"
 ALT="\includegraphics[width=10cm]{Figures/convert_ascii_netcdf_example.eps}">|; 

$key = q/{displaymath}r(t):=langlex(t)x(0)rangle.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="348" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img238.gif"
 ALT="\begin{displaymath}
r(t) :=\langle x(t)x(0)\rangle.
\end{displaymath}">|; 

$key = q/{{bf{r}';MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="21" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img204.gif"
 ALT="${\bf r}'$">|; 

$key = q/{displaymath}P_{ab}left(frac{n}{2N_t}right)=Deltatcdotsum_{m=-(N_t-1)}^{N_t-1}ex{2N_t}right)right],W(m),frac{1}{N-|m|}S_{AB}(m).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="556" HEIGHT="63" BORDER="0"
 SRC="|."$dir".q|img668.gif"
 ALT="\begin{displaymath}
P_{ab}\left(\frac{n}{2N_t}\right) =
\Delta t\cdot \sum_{m=-(...
...2N_t}\right)\right]
 W(m) \frac{1}{N-\vert m\vert}S_{AB}(m).
\end{displaymath}">|; 

$key = q/xi(mathbf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img477.gif"
 ALT="$\xi(\mathbf{q},t)$">|; 

$key = q/displaystylef_1(n)=Theta(n)cdotnf_2(n)=Theta(n)cdotC_{vv}(n);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="295" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img288.gif"
 ALT="$\displaystyle f_1(n)=\Theta(n)\cdot n
f_2(n)=\Theta(n)\cdot C_{vv}(n)$">|; 

$key = q/r_{alphabeta};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img533.gif"
 ALT="$r_{\alpha\beta}$">|; 

$key = q/{displaymath}CN_{gI}(r_m,t_f)=n_{gI}(r_m,t_f),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="385" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img561.gif"
 ALT="\begin{displaymath}
CN_{gI}(r_m, t_f) = n_{gI}(r_m, t_f),
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashpdf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="544" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img548.gif"
 ALT="\includegraphics[width=10cm]{Figures/pdf.eps}">|; 

$key = q/displaystyleleft{array{{ll}a(k)<col_mark>k=0ldotsN_t-1<row_mark>0<col_mark>k=N_tldots2N_t-1<row_mark>array{right.,;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="230" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img651.gif"
 ALT="$\displaystyle \left\{\begin{array}{ll}
a(k) &amp;k = 0\ldots N_t-1\\\\
0 &amp;k = N_t \ldots 2N_t-1\\\\
\end{array}\right.,$">|; 

$key = q/displaystyleleft{array{{ll}b(k)<col_mark>k=0ldotsN_t-1<row_mark>0<col_mark>k=N_tldots2N_t-1<row_mark>array{right.,;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="228" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img653.gif"
 ALT="$\displaystyle \left\{\begin{array}{ll}
b(k) &amp;k = 0\ldots N_t-1\\\\
0 &amp;k = N_t \ldots 2N_t-1\\\\
\end{array}\right.,$">|; 

$key = q/{displaymath}{{bf{R}'={{bf{Q}{{bf{R}{{bf{Q}^T,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="332" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img196.gif"
 ALT="\begin{displaymath}
{\bf R}' = {\bf Q}{\bf R}{\bf Q}^T,
\end{displaymath}">|; 

$key = q/A(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img656.gif"
 ALT="$A(k)$">|; 

$key = q/hbar{{bf{q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img439.gif"
 ALT="$\hbar {\bf q}$">|; 

$key = q/G_s({{bf{r},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img515.gif"
 ALT="$G_s({\bf r},t)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashgroup_selection_from_the_loaded_trajectory.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img75.gif"
 ALT="\includegraphics[width=10cm]{Figures/group_selection_from_the_loaded_trajectory.eps}">|; 

$key = q/proptoN_tlog_2(N_t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img639.gif"
 ALT="$\propto N_t \log_2(N_t)$">|; 

$key = q/{{bf{v}_2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img128.gif"
 ALT="${\bf v}_2$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdeuteration_selection.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img67.gif"
 ALT="\includegraphics[width=10cm]{Figures/deuteration_selection.eps}">|; 

$key = q/displaystyleoverline{b_alpha};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img47.gif"
 ALT="$\displaystyle \overline{b_\alpha}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashmsd_water.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="358" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img80.gif"
 ALT="\includegraphics[width=10cm]{Figures/msd_water.eps}">|; 

$key = q/p^1_{00}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img380.gif"
 ALT="$p^1_{0 0}(t)$">|; 

$key = q/alpha_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img332.gif"
 ALT="$\alpha_i$">|; 

$key = q/N_{triplets};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img136.gif"
 ALT="$N_{triplets}$">|; 

$key = q/{{bf{q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img222.gif"
 ALT="${\bf q}$">|; 

$key = q/r_{min};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img551.gif"
 ALT="$r_{min}$">|; 

$key = q/J;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img539.gif"
 ALT="$J$">|; 

$key = q/c^g;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img580.gif"
 ALT="$c^g$">|; 

$key = q/C_{vv}^{rangle}(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img294.gif"
 ALT="$C_{vv}^{\rangle}(z)$">|; 

$key = q/N_{phi};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img570.gif"
 ALT="$N_{\phi}$">|; 

$key = q/p({{bf{Omega}_1,t_1|{{bf{Omega}_0,t_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="121" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img341.gif"
 ALT="$p({\bf\Omega}_1,t_1\vert{\bf\Omega}_0,t_0)$">|; 

$key = q/displaystyle{calF}_{mathrm{coh}}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img424.gif"
 ALT="$\displaystyle {\cal F}_{\mathrm{coh}}({\bf q},t)$">|; 

$key = q/displaystyleGamma_{alphabeta};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img410.gif"
 ALT="$\displaystyle \Gamma_{\alpha\beta}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashgroup_selection_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="281" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img74.gif"
 ALT="\includegraphics[width=10cm]{Figures/group_selection_file.eps}">|; 

$key = q/{displaymath}textbf{d}_{alpha}(t,t_0)doteqtextbf{R}_alpha(t_0+t)-textbf{R}_alpha(t_0).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="401" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img81.gif"
 ALT="\begin{displaymath}
\textbf{d}_{\alpha}(t,t_0) \doteq \textbf{R}_\alpha(t_0 + t) - \textbf{R}_\alpha(t_0).
\end{displaymath}">|; 

$key = q/{displaymath}Xi^{(AR)}_{>}(z)=frac{1}{Deltat^2}left(frac{z}{mathrm{VACF}^{(AR)}_{>}(z)}+1-zright).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="441" HEIGHT="56" BORDER="0"
 SRC="|."$dir".q|img305.gif"
 ALT="\begin{displaymath}
\Xi^{(AR)}_{&gt;}(z) = \frac{1}{\Delta t^2}
\left(\frac{z}{\mathrm{VACF}^{(AR)}_{&gt;}(z)} + 1 - z\right).
\end{displaymath}">|; 

$key = q/{displaymath}A(k)=A(k+mcdot2N_t),qquadB(k)=B(k+mcdot2N_t),qquadm=0,pm1,pm2,ldots.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="557" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img655.gif"
 ALT="\begin{displaymath}
A(k) = A(k + m\cdot 2N_t),\qquad
B(k) = B(k + m\cdot 2N_t),\qquad m = 0,\pm 1,\pm 2,\ldots.
\end{displaymath}">|; 

$key = q/pm1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img455.gif"
 ALT="$\pm 1$">|; 

$key = q/displaystylesum_{k=0}^{N_t-m-1}textbf{r}(k)cdottextbf{r}(k+m).;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img103.gif"
 ALT="$\displaystyle \sum_{k=0}^{N_t-m-1} \textbf{r}(k)\cdot\textbf{r}(k+m).$">|; 

$key = q/{displaymath}mathrm{VACF}^{(AR)}(z)=frac{1}{a^{(P)}_P}frac{-z^Psigma_P^2}{prod_{k=1}^P(z-z_k),prod_{l=1}^P(z-z^{-1}_l)}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="471" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img245.gif"
 ALT="\begin{displaymath}
\mathrm{VACF}^{(AR)}(z) = \frac{1}{a^{(P)}_P}\frac{-z^P\sigma_P^2}
{\prod_{k=1}^P (z-z_k) \prod_{l=1}^P(z-z^{-1}_l)}.
\end{displaymath}">|; 

$key = q/theta_{min}:theta_{max}:theta_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img585.gif"
 ALT="$\theta_{min}:\theta_{max}:\theta_{step}$">|; 

$key = q/{displaymath}intdOmega,D^{*,j}_{mn}({{bf{Omega})D^{j'}_{m'n'}({{bf{Omega})=frac{8pi^2}{2j+1}delta_{jj'}delta_{mm'}delta_{nn'},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="455" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img352.gif"
 ALT="\begin{displaymath}
\int d\Omega D^{* j}_{m n}({\bf\Omega})D^{j'}_{m' n'}({\bf...
...)
= \frac{8\pi^2}{2j + 1}\delta_{jj'}\delta_{mm'}\delta_{nn'},
\end{displaymath}">|; 

$key = q/{displaymath}partial_t{mathcal{F}}_{mathrm{coh}}(mathbf{q},t)=-int_{0}^{t}dtau,x{mathcal{F}_{mathrm{coh}}}(mathbf{q},tau)mbox{.}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="444" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img476.gif"
 ALT="\begin{displaymath}
\partial_t{\mathcal{F}} _{\mathrm{coh}}(\mathbf{q},t) =
-\in...
...t-\tau){\mathcal{F} _{\mathrm{coh}}}(\mathbf{q},\tau) \mbox{.}
\end{displaymath}">|; 

$key = q/S^2_i(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img619.gif"
 ALT="$S^2_i(t)$">|; 

$key = q/displaystyleomega;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img402.gif"
 ALT="$\displaystyle \omega$">|; 

$key = q/Deltat;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img121.gif"
 ALT="$\Delta t$">|; 

$key = q/mathbf{I};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img318.gif"
 ALT="$\mathbf{I}$">|; 

$key = q/{displaymath}N=sum^{N_{species}}_{I=1}n_I{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="336" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img51.gif"
 ALT="\begin{displaymath}
N = \sum^{N_{species}}_{I = 1}n_I
\end{displaymath}">|; 

$key = q/c_j;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img278.gif"
 ALT="$c_j$">|; 

$key = q/displaystyleA(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img650.gif"
 ALT="$\displaystyle A(k)$">|; 

$key = q/omega_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img38.gif"
 ALT="$\omega_\alpha$">|; 

$key = q/displaystyle{{bf{x}_alpha-{{bf{X},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img182.gif"
 ALT="$\displaystyle {\bf x}_\alpha - {\bf X},$">|; 

$key = q/displaystyle2Deltat^2langlev^2ranglesum_{j=1}^Pbeta_jfrac{1}{2pii}ointdzfrac{z^{n+1}}{(z-1)^2}cdotfrac{1}{z-z_j};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="314" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img297.gif"
 ALT="$\displaystyle 2\Delta t^2\langle v^2\rangle \sum_{j=1}^P\beta_j \frac{1}{2\pi i}\oint dz \frac{z^{n+1}}{(z-1)^2}\cdot\frac{1}{z-z_j}$">|; 

$key = q/displaystyleMSD^{AR}(n);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img296.gif"
 ALT="$\displaystyle MSD^{AR}(n)$">|; 

$key = q/p({{bf{Omega}_1,t_1;{{bf{Omega}_0,t_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="124" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img343.gif"
 ALT="$p({\bf\Omega}_1,t_1;{\bf\Omega}_0,t_0)$">|; 

$key = q/{displaymath}omega_{alpha}=frac{Z_alpha}{sum_{alpha=1}^{N_{atoms}}Z_alpha}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="347" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img35.gif"
 ALT="\begin{displaymath}
\omega_{\alpha}=\frac{Z_\alpha}{\sum_{\alpha=1}^{N_{atoms}} Z_\alpha}
\end{displaymath}">|; 

$key = q/tildeAleft(frac{n}{2N_t}right);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img661.gif"
 ALT="$\tilde A\left(\frac{n}{2N_t}\right)$">|; 

$key = q/{displaymath}sigma_{ij}=leftlangleleft(x_{i}-langlexrangleright)left(x_{j}-langlexrangleright)rightrangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="385" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img314.gif"
 ALT="\begin{displaymath}
\sigma_{ij} = \left\langle \left ( x_{i} - \langle x \rangle \right ) \left ( x_{j} - \langle x \rangle \right )\right\rangle
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsubset_selection.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img62.gif"
 ALT="\includegraphics[width=10cm]{Figures/subset_selection.eps}">|; 

$key = q/{displaymath}left(array{{c}0omega'_xomega'_yomega'_zarray{right)=2cdotleft(arrayt(array{{c}dotq_0dotq_1dotq_2dotq_3array{right).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="472" HEIGHT="94" BORDER="0"
 SRC="|."$dir".q|img385.gif"
 ALT="\begin{displaymath}
\left(\begin{array}{c}
0 \\\\
\omega'_x \\\\
\omega'_y \\\\
...
...\
\dot q_1 \\\\
\dot q_2 \\\\
\dot q_3
\end{array}\right).
\end{displaymath}">|; 

$key = q/theta;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img404.gif"
 ALT="$\theta$">|; 

$key = q/p({{bf{Omega},0|{{bf{0},0)=delta({{bf{Omega});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="151" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img378.gif"
 ALT="$p({\bf\Omega},0\vert{\bf0},0) = \delta({\bf\Omega})$">|; 

$key = q/displaystylesum^{N_{species}}_{I=1}n_Iomega_{I,mathrm{inc}}{calF}_{I,mathrm{inc}}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="196" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img427.gif"
 ALT="$\displaystyle \sum^{N_{species}}_{I = 1}n_I \omega_{I,\mathrm{inc}}{\cal F}_{I,\mathrm{inc}}({\bf q},t)$">|; 

$key = q/displaystyleF^{g}_{I,alpha,mathrm{inc}}(q_m,kcdotDeltat);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="144" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img503.gif"
 ALT="$\displaystyle F^{g}_{I,\alpha ,\mathrm{inc}}(q_m,k\cdot\Delta t)$">|; 

$key = q/N_{frames};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img311.gif"
 ALT="$N_{frames}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashpbft.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="441" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img164.gif"
 ALT="\includegraphics[width=10cm]{Figures/pbft.eps}">|; 

$key = q/displaystyle{{bf{r}_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img181.gif"
 ALT="$\displaystyle {\bf r}_\alpha$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashscsf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="704" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img528.gif"
 ALT="\includegraphics[width=10cm]{Figures/scsf.eps}">|; 

$key = q/{{bf{r};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img194.gif"
 ALT="${\bf r}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsubset_selection_from_an_expression_string.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img66.gif"
 ALT="\includegraphics[width=10cm]{Figures/subset_selection_from_an_expression_string.eps}">|; 

$key = q/n_J;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img432.gif"
 ALT="$n_J$">|; 

$key = q/B(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img657.gif"
 ALT="$B(k)$">|; 

$key = q/phi_m=phi_{min}+mcdotphi_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="173" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img592.gif"
 ALT="$\phi_m = \phi_{min} + m \cdot \phi_{step}$">|; 

$key = q/{displaymath}m({{bf{q})=sum_alphaomega_alphaleft[{{bf{D}({{bf{q}){{bf{r}^{(0)}_alpha-{{bf{r}_alpharight]^2stackrel{!}{=}Min.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="431" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img180.gif"
 ALT="\begin{displaymath}
m({\bf q}) = \sum_\alpha \omega_\alpha
\left[{\bf D}({\bf q...
...r}^{(0)}_\alpha - {\bf r}_\alpha\right]^2
\stackrel{!}{=} Min.
\end{displaymath}">|; 

$key = q/{displaymath}rho_I({{bf{q},kcdotDeltat)=sum_alpha^{n_I}exp[i{{bf{q}cdot{{bf{R}_alpha(kcdotDeltat)].{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="426" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img447.gif"
 ALT="\begin{displaymath}
\rho_I({\bf q},k\cdot\Delta t) = \sum_\alpha^{n_I} \exp[i{\bf q}\cdot{\bf R}_\alpha(k\cdot\Delta t)].
\end{displaymath}">|; 

$key = q/displaystyle{{bf{q}cdot{{bf{q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img220.gif"
 ALT="$\displaystyle {\bf q}\cdot{\bf q}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdeuteration_selection_from_a_selection_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img68.gif"
 ALT="\includegraphics[width=10cm]{Figures/deuteration_selection_from_a_selection_file.eps}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashconvert_ascii_netcdf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="204" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img25.gif"
 ALT="\includegraphics[width=10cm]{Figures/convert_ascii_netcdf.eps}">|; 

$key = q/{displaymath}<r^2>=6Dt+C{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="350" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img78.gif"
 ALT="\begin{displaymath}
&lt;r^2&gt; = 6Dt + C
\end{displaymath}">|; 

$key = q/{{bf{X}^{(0)};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="23" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img170.gif"
 ALT="${\bf X}^{(0)}$">|; 

$key = q/{displaymath}MSD(n)=2sum_{k=-infty}^{+infty}Deltat^2f_1(n-k)f_2(k){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="424" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img289.gif"
 ALT="\begin{displaymath}
MSD(n)=2 \sum_{k=-\infty}^{+\infty} \Delta t^2f_1(n-k)f_2(k)
\end{displaymath}">|; 

$key = q/Z_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img42.gif"
 ALT="$Z_\alpha$">|; 

$key = q/{displaymath}array{{ll}&Y^j_m({{bf{q})=sqrt{frac{2j+1}{4pi}}sum_{p}(-1)^{p}frac{iq_3)^{j-p}(q_2+iq_1)^{p-m}(q_2-iq_1)^{p}.array{{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="512" HEIGHT="81" BORDER="0"
 SRC="|."$dir".q|img370.gif"
 ALT="\begin{displaymath}
\begin{array}{ll}
&amp;Y^j_m({\bf q}) = \sqrt{\frac{2j+1}{4\pi}...
...p}(q_0-iq_3)^{j-p}
(q_2+iq_1)^{p-m}(q_2-iq_1)^{p}.
\end{array}\end{displaymath}">|; 

$key = q/c^t_iqquadi=1,2ldotsN_{triplets};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="194" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img576.gif"
 ALT="$c^t_i \qquad i = 1, 2 \ldots N_{triplets}$">|; 

$key = q/{{bf{D}({{bf{q});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img172.gif"
 ALT="${\bf D}({\bf q})$">|; 

$key = q/textstylevdots;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img499.gif"
 ALT="$\textstyle \vdots$">|; 

$key = q/t=t_1-t_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img346.gif"
 ALT="$t = t_1-t_0$">|; 

$key = q/{displaymath}textbf{x}(t)=langletextbf{x}rangle+sum_{i=1}^{N_{modes}}alpha_iM^{-frac{1}{2}}Deltax_icos(omega_it){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="426" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img331.gif"
 ALT="\begin{displaymath}
\textbf{x}(t) = \langle \textbf{x} \rangle + \sum_{i=1}^{N_{modes}} \alpha_i M^{-\frac{1}{2}}\Delta x_i cos(\omega_i t)
\end{displaymath}">|; 

$key = q/{displaymath}{{bf{A}=left(array{{rrrr}a_0&-a_1&-a_2&-a_3a_1&a_0&-a_3&a_2a_2&a_3&a_0&-a_1a_3&-a_2&a_1&a_0array{right).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="402" HEIGHT="93" BORDER="0"
 SRC="|."$dir".q|img192.gif"
 ALT="\begin{displaymath}
{\bf A} = \left( \begin{array}{rrrr}
a_0 &amp;-a_1 &amp;-a_2 &amp;-a_3...
...&amp; a_0 &amp;-a_1 \\\\
a_3 &amp;-a_2 &amp; a_1 &amp; a_0
\end{array}
\right).
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashqha.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="396" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img335.gif"
 ALT="\includegraphics[width=10cm]{Figures/qha.eps}">|; 

$key = q/q1_x,q1_y,q1_z;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img472.gif"
 ALT="$q1_x,q1_y,q1_z$">|; 

$key = q/{displaymath}{{bf{R}=xcdot{{bf{I}+ycdot{{bf{J}+zcdot{{bf{K},{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="373" HEIGHT="30" BORDER="0"
 SRC="|."$dir".q|img195.gif"
 ALT="\begin{displaymath}
{\bf R} = x\cdot{\bf I} + y\cdot{\bf J} + z\cdot{\bf K},
\end{displaymath}">|; 

$key = q/sum_{I=1}^{N_{species}}{calW_I}=sum_{I=1}^{N_{species}}sum_{alpha=1}^{n_I}omega_{alpha,I}=1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="306" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img59.gif"
 ALT="$\sum_{I=1}^{N_{species}} {\cal W_I}=\sum_{I=1}^{N_{species}}\sum_{\alpha=1}^{n_I} \omega_{\alpha,I}= 1$">|; 

$key = q/nm^{-1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img475.gif"
 ALT="$nm^{-1}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashframe_snapshot.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="249" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img23.gif"
 ALT="\includegraphics[width=10cm]{Figures/frame_snapshot.eps}">|; 

$key = q/b_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img413.gif"
 ALT="$b_\alpha$">|; 

$key = q/{displaymath}{calS}_{mathrm{inc}}({{bf{q},omega)=EISF({{bf{q})delta(omega)+{calS}'_{mathrm{inc}}({{bf{q},omega).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="429" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img514.gif"
 ALT="\begin{displaymath}
{\cal S}_{\mathrm{inc}}({\bf q},\omega) = EISF({\bf q})\delta(\omega)
+ {\cal S}'_{\mathrm{inc}}({\bf q},\omega).
\end{displaymath}">|; 

$key = q/displaystyleS_{AA+BB}(m-1)-textbf{r}^2(m-1)-textbf{r}^2(N_t-m),;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="337" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img106.gif"
 ALT="$\displaystyle S_{AA+BB}(m-1) - \textbf{r}^2(m-1) - \textbf{r}^2(N_t - m),$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsubset_selection_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="281" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img64.gif"
 ALT="\includegraphics[width=10cm]{Figures/subset_selection_file.eps}">|; 

$key = q/{displaymath}p({{bf{Omega},t|{{bf{0},0)=sum_{jmn}frac{2j+1}{8pi^2},p^j_{mn}(t)D^{*,j}_{mn}({{bf{Omega}).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="433" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img350.gif"
 ALT="\begin{displaymath}
p({\bf\Omega},t\vert{\bf0},0) = \sum_{j m n} \frac{2j +
1}{8\pi^2} p^j_{m n}(t) D^{* j}_{m n}({\bf\Omega}).
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashopcm.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="356" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img621.gif"
 ALT="\includegraphics[width=10cm]{Figures/opcm.eps}">|; 

$key = q/{displaymath}omega_{alpha}=frac{1}{N_{atoms}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="330" HEIGHT="44" BORDER="0"
 SRC="|."$dir".q|img33.gif"
 ALT="\begin{displaymath}
\omega_{\alpha}=\frac{1}{N_{atoms}}
\end{displaymath}">|; 

$key = q/r;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img563.gif"
 ALT="$r$">|; 

$key = q/N_{residues};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img76.gif"
 ALT="$N_{residues}$">|; 

$key = q/{displaymath}p(z)=z^P-sum_{k=1}^{P}a^{(P)}_kz^{P-k}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img247.gif"
 ALT="\begin{displaymath}
p(z) = z^P-\sum_{k=1}^{P}a^{(P)}_k z^{P-k}.
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashamber_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img15.gif"
 ALT="\includegraphics[width=10cm]{Figures/amber_converter.eps}">|; 

$key = q/{displaymath}m({{bf{q})=sum_alphaomega_alpha|{{bf{Q}{{bf{R}^{(0)}_alpha-{{bf{R}_alpha{{bf{Q}|^2.stackrel{!}{=}Min.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="435" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img207.gif"
 ALT="\begin{displaymath}
m({\bf q}) = \sum_\alpha \omega_\alpha
\Vert{\bf Q}{\bf R}^{(0)}_\alpha - {\bf R}_\alpha{\bf Q}\Vert^2.
\stackrel{!}{=} Min.
\end{displaymath}">|; 

$key = q/theta_{max};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img590.gif"
 ALT="$\theta_{max}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashop.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="441" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img615.gif"
 ALT="\includegraphics[width=10cm]{Figures/op.eps}">|; 

$key = q/^{c,d,nomath_inline{dagnomath_inline{};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img5.gif"
 ALT="$^{c,d,\dag }$">|; 

$key = q/Delta;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img597.gif"
 ALT="$\Delta$">|; 

$key = q/(N_{frames},3N);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img336.gif"
 ALT="$(N_{frames},3N)$">|; 

$key = q/langlemathbf{x}rangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img312.gif"
 ALT="$\langle \mathbf{x} \rangle$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashpreferences_config_file_example.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="281" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img32.gif"
 ALT="\includegraphics[width=10cm]{Figures/preferences_config_file_example.eps}">|; 

$key = q/{{bf{M}_alpha;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img213.gif"
 ALT="${\bf M}_\alpha$">|; 

$key = q/vec{v_3}=vec{v_1}timesvec{v_2};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img133.gif"
 ALT="$\vec{v_3} = \vec{v_1} \times \vec{v_2}$">|; 

$key = q/C_{vv;alphaalpha}(t;{{bf{n}_q);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img148.gif"
 ALT="$C_{vv ; \alpha\alpha}(t;{\bf n}_q)$">|; 

$key = q/{displaymath}langleomegarangle=frac{hbarq^2}{2M}:,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="325" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img438.gif"
 ALT="\begin{displaymath}
\langle\omega\rangle = \frac{\hbar q^2}{2M}\:,
\end{displaymath}">|; 

$key = q/{displaymath}d_{alpha}(t,t_0;textbf{n})doteqtextbf{n}cdottextbf{d}_{alpha}(t,t_0).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="376" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img88.gif"
 ALT="\begin{displaymath}
d_{\alpha}(t,t_0;\textbf{n}) \doteq \textbf{n}\cdot \textbf{d}_{\alpha}(t,t_0).
\end{displaymath}">|; 

$key = q/displaystylep^{j}_{-m-n}(t)=p^j_{nm}(-t).;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="168" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img375.gif"
 ALT="$\displaystyle p^{j}_{-m -n}(t) = p^j_{n m}(-t).$">|; 

$key = q/C^I_{ii}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img608.gif"
 ALT="$C^I_{ii}(t)$">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_zoom_to_rect.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img629.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_zoom_to_rect.eps}">|; 

$key = q/displaystylefrac{z^{-N}}{Deltat^2},frac{sum_{j=0}^N,left[VACF(j)-VACF(j+1)right],z^{N-j}}{sum_{j=0}^N,VACF(j),z^{-j}};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="354" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img274.gif"
 ALT="$\displaystyle \frac{z^{-N}}{\Delta t^2} \frac{\sum_{j=0}^N   \left[VACF(j) -
VACF(j+1)\right]   z^{N-j}} { \sum_{j=0}^N   VACF(j)  
z^{-j}}$">|; 

$key = q/displaystylesum_{alpha}omega_alphatildeC_{vv;alphaalpha}(ncdotDeltanu),qquadn=0ldotsN_t-1.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="328" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img157.gif"
 ALT="$\displaystyle \sum_{\alpha} \omega_\alpha
\tilde C_{vv ; \alpha\alpha}(n\cdot\Delta \nu),\qquad n = 0\ldots N_t - 1.$">|; 

$key = q/{displaymath}frac{VACF(n+1)-VACF(n)}{Deltat}=-sum_{k=0}^{n-1}Deltat,xi(n-k)psi(k),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="484" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img262.gif"
 ALT="\begin{displaymath}
\frac{VACF(n+1)-VACF(n)}{\Delta t} = -\sum_{k=0}^{n-1}
\Delta t \xi(n-k)\psi(k),
\end{displaymath}">|; 

$key = q/P_2(.);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img604.gif"
 ALT="$P_2(.)$">|; 

$key = q/T=(N_t-1)cdotDeltat;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="139" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img641.gif"
 ALT="$T = (N_t-1)\cdot\Delta t$">|; 

$key = q/{displaymath}langleT^j_m(t)T^{*,j}_n(0)rangle=p^j_{mn}(t)cdotfrac{1}{2j+1}sum_{l}|hatT^j_l|^2,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="438" HEIGHT="51" BORDER="0"
 SRC="|."$dir".q|img364.gif"
 ALT="\begin{displaymath}
\langle T^j_m(t) T^{* j}_n(0) \rangle =
p^j_{m n}(t) \cdot \frac{1}{2j + 1} \sum_{l}\vert\hat T^j_l\vert^2,
\end{displaymath}">|; 

$key = q/{displaymath}Delta^2_alpha(t)=leftlangletextbf{d}^2_alpha(t,t_0)rightrangle_{t_0}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="359" HEIGHT="39" BORDER="0"
 SRC="|."$dir".q|img82.gif"
 ALT="\begin{displaymath}
\Delta^2_\alpha(t) = \left\langle \textbf{d}^2_\alpha(t,t_0) \right\rangle_{t_0}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashac.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="441" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img137.gif"
 ALT="\includegraphics[width=10cm]{Figures/ac.eps}">|; 

$key = q/1slashV;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img519.gif"
 ALT="$1/V$">|; 

$key = q/displaystyleDOS(ncdotDeltanu);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img155.gif"
 ALT="$\displaystyle DOS(n\cdot\Delta \nu)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdcsfar.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="744" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img480.gif"
 ALT="\includegraphics[width=10cm]{Figures/dcsfar.eps}">|; 

$key = q/p({{bf{Omega}_1,t_1|{{bf{Omega}_0,t_0)=p({{bf{Omega},t|{{bf{0},0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="226" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img347.gif"
 ALT="$p({\bf\Omega}_1,t_1\vert{\bf\Omega}_0,t_0) = p({\bf\Omega},t\vert{\bf0},0)$">|; 

$key = q/{calF}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img407.gif"
 ALT="${\cal F}({\bf q},t)$">|; 

$key = q/theta_{min};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img587.gif"
 ALT="$\theta_{min}$">|; 

$key = q/a_nu;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img193.gif"
 ALT="$a_\nu$">|; 

$key = q/{displaymath}v_alpha(t;{{bf{n})doteq{{bf{n}cdot{{bf{v}_{alpha}(t).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="354" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img141.gif"
 ALT="\begin{displaymath}
v_\alpha(t;{\bf n}) \doteq
{\bf n}\cdot{\bf v}_{\alpha}(t).
\end{displaymath}">|; 

$key = q/t_1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img339.gif"
 ALT="$t_1$">|; 

$key = q/^{c,d};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.gif"
 ALT="$^{c,d}$">|; 

$key = q/psi(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img243.gif"
 ALT="$\psi(t)$">|; 

$key = q/{displaymath}{MSD}^{>}(z)=2F_1^{>}F_2^{>}Deltat^2,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img290.gif"
 ALT="\begin{displaymath}
{MSD}^{&gt;}(z)= 2 F_1^{&gt;} F_2^{&gt;}\Delta t^2 ,
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdeuteration_selection_from_the_loaded_trajectory.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img70.gif"
 ALT="\includegraphics[width=10cm]{Figures/deuteration_selection_from_the_loaded_trajectory.eps}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashgroup_selection_from_an_expression_string.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img77.gif"
 ALT="\includegraphics[width=10cm]{Figures/group_selection_from_an_expression_string.eps}">|; 

$key = q/{{bf{X};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img169.gif"
 ALT="${\bf X}$">|; 

$key = q/psi(t)=psi(-t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img283.gif"
 ALT="$\psi(t)=\psi(-t)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashtrajectory_set.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="330" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img13.gif"
 ALT="\includegraphics[width=10cm]{Figures/trajectory_set.eps}">|; 

$key = q/t_{ref};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img119.gif"
 ALT="$t_{ref}$">|; 

$key = q/{displaymath}gci=sum_{j=1}^{3N}frac{|Deltax_{ji}|}{sqrt{3N}}^{-4}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="351" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img325.gif"
 ALT="\begin{displaymath}
gci = \sum_{j = 1}^{3N} \frac{\vert\Delta x_{ji}\vert}{\sqrt{3N}}^{-4}
\end{displaymath}">|; 

$key = q/{displaymath}p^j_{mn}(t)=4pilangleY^j_m[{{bf{q}(t)]Y^{*j}_n[{{bf{q}(0)]rangle.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="401" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img371.gif"
 ALT="\begin{displaymath}
p^j_{m n}(t) = 4\pi \langle Y^j_m[{\bf q}(t)]Y^{* j}_n[{\bf q}(0)]\rangle.
\end{displaymath}">|; 

$key = q/sqrt{3N};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img328.gif"
 ALT="$\sqrt{3N}$">|; 

$key = q/PDF_{alphabeta}(r);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img538.gif"
 ALT="$PDF_{\alpha\beta}(r)$">|; 

$key = q/displaystyleS_{AA+BB}(m);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img100.gif"
 ALT="$\displaystyle S_{AA+BB}(m)$">|; 

$key = q/{calF}_{mathrm{inc}}'({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img513.gif"
 ALT="${\cal F}_{\mathrm{inc}}'({\bf q},t)$">|; 

$key = q/{displaymath}beta_j=frac{1}{a_P}frac{-z_j^{P-1}sigma_P^2}{prod_{k=1,knej}^P(z_j-z_k),prod_{l=1}^P(z_j-z^{-1}_l)}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="443" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img253.gif"
 ALT="\begin{displaymath}
\beta_j = \frac{1}{a_P}\frac{-z_j^{P-1}\sigma_P^2}
{\prod_{k=1,k\ne j}^P (z_j-z_k) \prod_{l=1}^P(z_j-z^{-1}_l)}.
\end{displaymath}">|; 

$key = q/S^2_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img612.gif"
 ALT="$S^2_i$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashrcf.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="521" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img382.gif"
 ALT="\includegraphics[width=10cm]{Figures/rcf.eps}">|; 

$key = q/{{bf{r}_{alpha}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img116.gif"
 ALT="${\bf r}_{\alpha}(t)$">|; 

$key = q/overline{rule{0pt}{5pt}ldots}^{q};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img448.gif"
 ALT="$\overline{\rule{0pt}{5pt}\ldots}^{q}$">|; 

$key = q/displaystylerho_{alpha,1}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img495.gif"
 ALT="$\displaystyle \rho_{\alpha,1}(t)$">|; 

$key = q/{displaymath}x(t)=sum_{n=1}^Pa^{(P)}_nx(t-nDeltat)+epsilon_P(t).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="409" HEIGHT="58" BORDER="0"
 SRC="|."$dir".q|img231.gif"
 ALT="\begin{displaymath}
x(t) = \sum_{n=1}^P a^{(P)}_n x(t- n \Delta t) + \epsilon_P(t).
\end{displaymath}">|; 

$key = q/9N^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img327.gif"
 ALT="$9N^2$">|; 

$key = q/displaystyle2Deltat^2langlev^2ranglesum_{j=1}^Pbeta_j{frac{n}{1-z_j}-frac{z_j}{(1-z_j)^2}+frac{z_j^{n+1}}{(1-z_j)^2}}.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="374" HEIGHT="72" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img298.gif"
 ALT="$\displaystyle 2\Delta t^2\langle v^2\rangle \sum_{j=1}^P\beta_j \{\frac{n}{1-z_j}-\frac{z_j}{(1-z_j)^2}+\frac{z_j^{n+1}}{(1-z_j)^2}\}.$">|; 

$key = q/hbar;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img399.gif"
 ALT="$\hbar$">|; 

$key = q/{displaymath}langleT^j_m(t_1)T^{*,j}_n(t_0)rangle=intintdOmega_1dOmega_0,p({{bf{_0)T^j_m({{bf{Omega}_1)T^{*,j}_n({{bf{Omega}_0),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="524" HEIGHT="42" BORDER="0"
 SRC="|."$dir".q|img358.gif"
 ALT="\begin{displaymath}
\langle T^j_m(t_1) T^{* j}_n(t_0) \rangle =
\int\int d\Omeg...
...f\Omega}_0,t_0)
T^j_m({\bf\Omega}_1)T^{* j}_n({\bf\Omega}_0),
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashnamd_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="295" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img21.gif"
 ALT="\includegraphics[width=10cm]{Figures/namd_converter.eps}">|; 

$key = q/{displaymath}Xi_{>}(z)=frac{1}{Deltat^2}left(frac{z}{mathrm{VACF}_{>}(z)}+1-zright),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="421" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img264.gif"
 ALT="\begin{displaymath}
\Xi_{&gt;}(z) = \frac{1}{\Delta t^2} \left(\frac{z}{\mathrm{VACF}_{&gt;}(z)} + 1 -
z\right),
\end{displaymath}">|; 

$key = q/x(n)equivx(nDeltat);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img230.gif"
 ALT="$x(n) \equiv x(n\Delta t)$">|; 

$key = q/Xi_{>}(z)mathrm{VACF}_{>}(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="133" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img267.gif"
 ALT="$\Xi_{&gt;}(z)\mathrm{VACF}_{&gt;}(z)$">|; 

$key = q/{calR}^t;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img581.gif"
 ALT="${\cal R}^t$">|; 

$key = q/phi_{min}:phi_{max}:phi_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="145" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img591.gif"
 ALT="$\phi_{min}:\phi_{max}:\phi_{step}$">|; 

$key = q/displaystylefrac{1}{2!}langled^2_alpha(t;textbf{n}_q)rangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="57" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img496.gif"
 ALT="$\displaystyle \frac{1}{2!}
\langle d^2_\alpha(t;\textbf{n}_q) \rangle$">|; 

$key = q/r^O_{i-1,k};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img617.gif"
 ALT="$r^O_{i-1,k}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashgmft.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="401" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img166.gif"
 ALT="\includegraphics[width=10cm]{Figures/gmft.eps}">|; 

$key = q/{displaymath}T^j_m({{bf{Omega}_1)=sum_kD^{j}_{mk}({{bf{Omega})T^j_k({{bf{Omega}_0).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="395" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img360.gif"
 ALT="\begin{displaymath}
T^j_m({\bf\Omega}_1) = \sum_k D^{j}_{m k}({\bf\Omega})
T^j_k({\bf\Omega}_0).
\end{displaymath}">|; 

$key = q/{a_n};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img239.gif"
 ALT="$\{a_n\}$">|; 

$key = q/{z_k};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img246.gif"
 ALT="$\{z_k\}$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashsubset_selection_from_a_selection_file.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img63.gif"
 ALT="\includegraphics[width=10cm]{Figures/subset_selection_from_a_selection_file.eps}">|; 

$key = q/(q,t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img506.gif"
 ALT="$(q,t)$">|; 

$key = q/omega_I;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img445.gif"
 ALT="$\omega_I$">|; 

$key = q/textbf{d}_alpha(t)=int_{0}^{t}dtau,textbf{v}_alpha(tau);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.gif"
 ALT="$\textbf{d}_\alpha(t) = \int_{0}^{t}d\tau \textbf{v}_\alpha(\tau)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashcharmm_converter.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="249" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img16.gif"
 ALT="\includegraphics[width=10cm]{Figures/charmm_converter.eps}">|; 

$key = q/includegraphics[width=10cm]{Figuresslashbenchmark.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="266" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img634.gif"
 ALT="\includegraphics[width=10cm]{Figures/benchmark.eps}">|; 

$key = q/sum_{j=1}^{3N}Deltax_{ji}^2=1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img323.gif"
 ALT="$\sum_{j = 1}^{3N} \Delta x_{ji}^2 = 1$">|; 

$key = q/displaystyle{calF}_{coh}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img420.gif"
 ALT="$\displaystyle {\cal F}_{coh}({\bf q},t)$">|; 

$key = q/{displaymath}p({{bf{Omega},t|{{bf{0},0)=frac{1}{N}sum_alphalangledelta[{{bf{Omega}-{{bf{Omega}_alpha(t)]rangle,{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="414" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img354.gif"
 ALT="\begin{displaymath}
p({\bf\Omega},t\vert{\bf0},0) = \frac{1}{N}
\sum_\alpha \langle\delta[{\bf\Omega} - {\bf\Omega}_\alpha(t)]\rangle,
\end{displaymath}">|; 

$key = q/{displaymath}{{bf{r}'={{bf{D}({{bf{q}){{bf{r}.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="324" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img205.gif"
 ALT="\begin{displaymath}
{\bf r}' = {\bf D}({\bf q}){\bf r}.
\end{displaymath}">|; 

$key = q/{{bf{Omega};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img342.gif"
 ALT="${\bf\Omega}$">|; 

$key = q/N_{theta};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img569.gif"
 ALT="$N_{\theta}$">|; 

$key = q/Xi_{>}(z);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img268.gif"
 ALT="$\Xi_{&gt;}(z)$">|; 

$key = q/a(k);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img642.gif"
 ALT="$a(k)$">|; 

$key = q/includegraphics[width=10cm]{Figuresslashdlpoly_field_example.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="453" HEIGHT="330" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img18.gif"
 ALT="\includegraphics[width=10cm]{Figures/dlpoly_field_example.eps}">|; 

$key = q/delta^j_{mn};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img376.gif"
 ALT="$\delta^j_{mn}$">|; 

$key = q/displaystylep^{*j}_{mn}(t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img374.gif"
 ALT="$\displaystyle p^{* j}_{m n}(t)$">|; 

$key = q/^{b};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img2.gif"
 ALT="$^{b}$">|; 

$key = q/displaystylesum_{alpha,beta}Gamma_{alphabeta}langleexp[-i{{bf{q}cdothat{{bf{R}_alpha(0)]exp[i{{bf{q}cdothat{{bf{R}_beta(t)]rangle,;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="317" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img409.gif"
 ALT="$\displaystyle \sum_{\alpha,\beta}\Gamma_{\alpha\beta}
\langle\exp[-i{\bf q}\cdot\hat{\bf R}_\alpha(0)]
\exp[i{\bf q}\cdot\hat{\bf R}_\beta(t)]\rangle,$">|; 

$key = q/textbf{R}_alpha(t_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img83.gif"
 ALT="$\textbf{R}_\alpha(t_0)$">|; 

$key = q/N_q;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img444.gif"
 ALT="$N_q$">|; 

$key = q/{displaymath}MSD^{>}(z)=2frac{zDeltat^2}{(z-1)^2}C_{vv}^{>}(z){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="392" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img292.gif"
 ALT="\begin{displaymath}
MSD^{&gt;}(z)= 2 \frac{z\Delta t^2}{(z-1)^2}C_{vv}^{&gt;}(z)
\end{displaymath}">|; 

$key = q/[Omega,Omega+dOmega];MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img391.gif"
 ALT="$[\Omega,\Omega+d\Omega]$">|; 

$key = q/displaystyledelta^j_{mn},;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img373.gif"
 ALT="$\displaystyle \delta^j_{mn},$">|; 

$key = q/{displaymath}{calW_I}=frac{n_{I}Z_{I}}{sum^{N_{species}}_{I=1}n_{I}Z_{I}}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="360" HEIGHT="52" BORDER="0"
 SRC="|."$dir".q|img56.gif"
 ALT="\begin{displaymath}
{\cal W_I}=\frac{n_{I}Z_{I}}{\sum^{N_{species}}_{I=1} n_{I}Z_{I}}
\end{displaymath}">|; 

$key = q/includegraphics[width=10cm]{Figuresslasheffective_mode_viewer.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="451" HEIGHT="408" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img633.gif"
 ALT="\includegraphics[width=10cm]{Figures/effective_mode_viewer.eps}">|; 

$key = q/t_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img85.gif"
 ALT="$t_0$">|; 

$key = q/t;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img118.gif"
 ALT="$t$">|; 

$key = q/F(z)=sum_{n=-infty}^{+infty}f(n)z^{-n};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="192" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img249.gif"
 ALT="$F(z) = \sum_{n=-\infty}^{+\infty} f(n) z^{-n}$">|; 

$key = q/{displaymath}SD(r_l,theta_m,phi_n)doteqfrac{1}{N_{tripletsN_{groups}}}sum_{t=1}^dotsN_r-1,m=0ldotsN_{theta}-1,n=0ldotsN_{phi}-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="876" HEIGHT="63" BORDER="0"
 SRC="|."$dir".q|img565.gif"
 ALT="\begin{displaymath}
SD(r_l,\theta_m ,\phi_n) \doteq \frac{1}{N_{triplets N_{grou...
...N_r - 1, m = 0\ldots N_{\theta} - 1, n = 0\ldots N_{\phi} - 1.
\end{displaymath}">|; 

$key = q/{displaymath}S_{AB}(m)=sum_{k=0}^{2N_t-1}A^*(k)B(k+m).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="405" HEIGHT="59" BORDER="0"
 SRC="|."$dir".q|img658.gif"
 ALT="\begin{displaymath}
S_{AB}(m) = \sum_{k=0}^{2N_t-1} A^*(k)B(k+m).
\end{displaymath}">|; 

$key = q/a_n^{(P)};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img309.gif"
 ALT="$a_n^{(P)}$">|; 

$key = q/{displaymath}EISF({{bf{q})=sum^{N_{species}}_{I=1}n_Iomega_{I,mathrm{inc}}EISF_I(q){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="420" HEIGHT="61" BORDER="0"
 SRC="|."$dir".q|img520.gif"
 ALT="\begin{displaymath}
EISF({\bf q}) = \sum^{N_{species}}_{I = 1} n_I \omega_{I,\mathrm{inc}} EISF_I(q)
\end{displaymath}">|; 

$key = q/q_m=q_{min}+mcdotq_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="166" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img465.gif"
 ALT="$q_m = q_{min} + m \cdot q_{step}$">|; 

$key = q/displaystylesum_alphaomega_alpha,{{bf{x}^{(0)}_alpha.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img179.gif"
 ALT="$\displaystyle \sum_\alpha \omega_\alpha {\bf x}^{(0)}_\alpha.$">|; 

$key = q/{displaymath}{{bf{b}_i{{bf{b}^j=delta_i^j.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="319" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img457.gif"
 ALT="\begin{displaymath}
{\bf b}_i{\bf b}^j = \delta_i^j.
\end{displaymath}">|; 

$key = q/q1_x,q1_y,q1_z;q2_x,q2_y,q2_z;ldots;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="224" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img474.gif"
 ALT="$q1_x,q1_y,q1_z;q2_x,q2_y,q2_z;\ldots$">|; 

$key = q/{{bf{b}^1,{{bf{b}^2,{{bf{b}^3;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img456.gif"
 ALT="${\bf b}^1,{\bf b}^2,{\bf b}^3$">|; 

$key = q/{displaymath}ROG(ncdotDeltat)=sqrt{frac{sum_{alpha=1}^{N_{alpha}}({{bf{r}_{alphabf{r}_{cms}(t))}{N_{alpha}}},qquadn=0ldotsN_t-1.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="517" HEIGHT="55" BORDER="0"
 SRC="|."$dir".q|img125.gif"
 ALT="\begin{displaymath}
ROG(n\cdot\Delta t) = \sqrt{\frac{\sum_{\alpha = 1}^{N_{\alp...
...- {\bf r}_{cms}(t))}{N_{\alpha}}},
\qquad n = 0\ldots N_t - 1.
\end{displaymath}">|; 

$key = q/{displaymath}left.array{{lll}M_{alpha,11}&=&x_alpha^2+y_alpha^2+z_alpha^2+x_{0aly_alphay_{0alpha}-2z_alphaz_{0alpha}array{right}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="580" HEIGHT="223" BORDER="0"
 SRC="|."$dir".q|img216.gif"
 ALT="\begin{displaymath}
\left.
\begin{array}{lll}
M_{\alpha,11} &amp;= &amp;
x_\alpha^2 + y...
...pha y_{0\alpha} -2z_\alpha z_{0\alpha}
\\\\
\end{array}\right\}
\end{displaymath}">|; 

$key = q/phi_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img594.gif"
 ALT="$\phi_{step}$">|; 

$key = q/({{bf{Omega}_1,{{bf{Omega}_0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img362.gif"
 ALT="$({\bf\Omega}_1,{\bf\Omega}_0)$">|; 

$key = q/displaystyleS_{AA+BB}(m)-2S_{AB}(m),;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img99.gif"
 ALT="$\displaystyle S_{AA+BB}(m) - 2 S_{AB}(m),$">|; 

$key = q/j;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img366.gif"
 ALT="$j$">|; 

$key = q/N_{alpha};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img115.gif"
 ALT="$N_{\alpha}$">|; 

$key = q/{displaymath}D_alpha=pitildeC_{vv;alphaalpha}(0).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img93.gif"
 ALT="\begin{displaymath}
D_\alpha = \pi \tilde C_{vv ; \alpha\alpha}(0).
\end{displaymath}">|; 

$key = q/R;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img279.gif"
 ALT="$R$">|; 

$key = q/z^{-N};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="22" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img272.gif"
 ALT="$z^{-N}$">|; 

$key = q/{displaymath}EISF({{bf{q})doteqlim_{ttoinfty}{calF}_{mathrm{inc}}({{bf{q},t).{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="382" HEIGHT="39" BORDER="0"
 SRC="|."$dir".q|img511.gif"
 ALT="\begin{displaymath}
EISF({\bf q}) \doteq \lim_{t \to \infty} {\cal F}_{\mathrm{inc}}({\bf q},t).
\end{displaymath}">|; 

$key = q/v_alpha(t;{{bf{n});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img140.gif"
 ALT="$v_\alpha(t;{\bf n})$">|; 

$key = q/{displaymath}AC_{g,i}(t)=langle{{bf{v}_{t,i}(0)cdot{{bf{v}_{t,i}(t)rangle,qquadi=1,2,3{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="438" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img134.gif"
 ALT="\begin{displaymath}
AC_{g,i} (t) = \langle {\bf v}_{t,i}(0)\cdot{\bf v}_{t,i}(t)\rangle, \qquad i = 1,2,3
\end{displaymath}">|; 

$key = q/displaystyle{calF}_{IJ,mathrm{coh}}({{bf{q},t);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img428.gif"
 ALT="$\displaystyle {\cal F}_{IJ,\mathrm{coh}}({\bf q},t)$">|; 

$key = q/theta_{step};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img588.gif"
 ALT="$\theta_{step}$">|; 

$key = q/{displaymath}omega_{alpha}=frac{b_{alpha,mathrm{inc}}^2}{sum_{alpha=1}^{N_{atoms}}b_{alpha,mathrm{inc}}^2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="57" BORDER="0"
 SRC="|."$dir".q|img36.gif"
 ALT="\begin{displaymath}
\omega_{\alpha}=\frac{b_{\alpha,\mathrm{inc}}^2}{\sum_{\alpha=1}^{N_{atoms}} b_{\alpha,\mathrm{inc}}^2}
\end{displaymath}">|; 

$key = q/{displaymath}{calF}^{g}_{I,mathrm{inc}}(textbf{q},t)=frac{1}{n_I}sum^{n_I}_{alphq^2rho_{alpha,1}(t)+q^4rho_{alpha,2}(t)mpldots].{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="480" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img493.gif"
 ALT="\begin{displaymath}
{\cal F}^{g}_{I,\mathrm{inc}}(\textbf{q},t) = \frac{1}{n_I}\...
...exp[-q^2\rho_{\alpha,1}(t) + q^4\rho_{\alpha,2}(t) \mp\ldots].
\end{displaymath}">|; 

$key = q/t=0.;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img285.gif"
 ALT="$t=0.$">|; 

$key = q/includegraphics[width=1cm]{Figuresslashmatplotlib_filesave.eps};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="43" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img631.gif"
 ALT="\includegraphics[width=1cm]{Figures/matplotlib_filesave.eps}">|; 

$key = q/{{bf{q}cdot{{bf{q}=1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img174.gif"
 ALT="${\bf q}\cdot{\bf q} = 1$">|; 

$key = q/omega_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img333.gif"
 ALT="$\omega_i$">|; 

$key = q/{displaymath}{calS}({{bf{q},omega)=exp[betahbaromega]{calS}(-{{bf{q},-omega),{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="398" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img436.gif"
 ALT="\begin{displaymath}
{\cal S}({\bf q},\omega) = \exp[\beta\hbar\omega]
{\cal S}(-{\bf q},-\omega),
\end{displaymath}">|; 

1;

